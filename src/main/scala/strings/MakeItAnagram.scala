package strings

/**
 * Alice recently started learning about cryptography and found that anagrams
 * are very useful. Two strings are anagrams of each other if they have same
 * character set and same length. For example strings "bacdc" and "dcbac" are
 * anagrams, while strings "bacdc" and "dcbad" are not.
 *
 * Alice decides on an encryption scheme involving 2 large strings where
 * encryption is dependent on the minimum number of character deletions required
 * to make the two strings anagrams. She need your help in finding out this
 * number.
 *
 * Given two strings (they can be of same or different length) help her in
 * finding out the minimum number of character deletions required to make two
 * strings anagrams. Any characters can be deleted from any of the strings.
 *
 * @see https://www.hackerrank.com/challenges/make-it-anagram
 */
object MakeItAnagram {
  
  def makeAnagram(as: List[Char], bs: List[Char]): (Int, List[Char]) = {
    val excluded = as diff bs
    val anagram = as diff excluded
    val n = as.size + bs.size - (2 * anagram.size)
    (n, anagram)
  }

  def main(args: Array[String]) {
    val lines = io.Source.stdin.getLines().toArray
    printResult(lines)
  }

  def printResult(lines: Array[String]): Unit = {
    val Array(a, b) = lines
    val result = makeAnagram(a.toList, b.toList)
    println(result._1)
  }
  
}