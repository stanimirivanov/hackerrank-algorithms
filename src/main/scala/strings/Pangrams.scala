package strings

/**
 * Roy wanted to increase his typing speed for programming contests.
 * So, his friend advised him to type the sentence "The quick brown
 * fox jumps over the lazy dog" repeatedly because it is a pangram.
 *
 * ( pangrams are sentences constructed by using every letter of the alphabet at least once. )
 *
 * After typing the sentence several times, Roy became bored with it.
 * So he started to look for other pangrams.
 *
 * Given a sentence s, tell Roy if it is a pangram or not.
 *
 * @see https://www.hackerrank.com/challenges/pangrams
 */
object Pangrams {
  
  def isPangram(sentence: String): Boolean =
    sentence.toLowerCase.filter(c => c >= 'a' && c <= 'z').toSet.size == 26

  def main(args: Array[String]) {
    val lines = io.Source.stdin.getLines().toList
    printResult(lines.head)
  }

  def printResult(sentence: String): Unit = {
    val result = isPangram(sentence)
    if (result) println("pangram") else println("not pangram")
  }
  
}