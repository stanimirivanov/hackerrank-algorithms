package strings

/**
 * AGiven a string S, find the number of unordered anagramic pairs of substrings.
 *
 * @see https://www.hackerrank.com/challenges/sherlock-and-anagrams
 * @see https://en.wikipedia.org/wiki/Unordered_pair
 * @see http://en.wikipedia.org/wiki/Anagram
 *
 */
object SherlockAndAnagrams {

  object PrimeHash {

    /*
    * Primes are sorted by letter frequency: Frequently occurring letters (i.e. "E") get smaller prime numbers
    * Minimizes memory footprint and prevents integer overflow for very large Anagrams.
    * Details:
    * http://en.wikipedia.org/wiki/Letter_frequency
    */
    private[this] final val primeAlphabet: Map[Char, Int] = Map(
      'e'	->	2,
      't'	->	3,
      'a'	->	5,
      'o'	->	7,
      'i'	->	11,
      'n'	->	13,
      's'	->	17,
      'h'	->	19,
      'r'	->	23,
      'd'	->	29,
      'l'	->	31,
      'c'	->	37,
      'u'	->	41,
      'm'	->	43,
      'w'	->	47,
      'f'	->	53,
      'g'	->	59,
      'y'	->	61,
      'p'	->	67,
      'b'	->	71,
      'v'	->	73,
      'k'	->	79,
      'j'	->	83,
      'x'	->	89,
      'q'	->	97,
      'z'	->	101
    )

    def hashOf(s: String): Int = {
      s.foldLeft(1) { (hash, character) => hash * primeAlphabet(character) }
    }
  }

  def anagramicPairs(word: String): Seq[(String, String)] = {
    val substrings = word.inits.flatMap(_.tails.toVector.init).toVector
    val byLength = (substrings groupBy(s => s.length)) - word.length
    val pairs = byLength.values flatMap { combinations(_) }
    val result = pairs filter { case (a, b) => anagrams(a, b)}
    result.toSeq
  }

  def anagrams(s: String, t: String): Boolean =
    PrimeHash.hashOf(s).equals(PrimeHash.hashOf(t))

  def combinations(list: Vector[String]): Seq[(String, String)] = {
    for {
      i <- 0 until list.size
      j <- i + 1 until list.size
    } yield (list(i), list(j))
  }

  def main(args: Array[String]) {
    val lines = io.Source.stdin.getLines().toArray
    lines.tail foreach { printResult(_) }
  }

  def printResult(line: String): Unit = {
    val result = anagramicPairs(line)
    println(result.size)
  }

}