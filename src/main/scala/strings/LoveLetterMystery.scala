package strings

/**
 * James found a love letter his friend Harry has written for his girlfriend.
 * James is a prankster, so he decides to meddle with the letter.
 * He changes all the words in the letter into palindromes.
 *
 * To do this, he follows 2 rules:
 *
 * (a) He can reduce the value of a letter, e.g. he can change 'd' to 'c',
 *     but he cannot change 'c' to 'd'.
 * (b) In order to form a palindrome, if he has to repeatedly reduce the
 *     value of a letter, he can do it until the letter becomes 'a'.
 *     Once a letter has been changed to 'a', it can no longer be changed.
 *
 * Each reduction in the value of any letter is counted as a single operation.
 * Find the minimum number of operations required to convert a given string into a palindrome.
 *
 * @see http://www.hackerrank.com/challenges/the-love-letter-mystery
 */
object LoveLetterMystery {

  def swapsCount(word: String): Int = {
    val cs = word.toList
    val i = math.floor(cs.size / 2).toInt
    val firstHalf = cs.take(i)
    val lastHalfReversed = cs.reverse.take(i)
    firstHalf.zip(lastHalfReversed).foldLeft(0) {
      (acc, t) => acc + math.abs(t._1 - t._2)
    }
  }

  def main(args: Array[String]) {
    val lines = io.Source.stdin.getLines().toList
    lines.tail.foreach(line => printResult(line))
  }

  def printResult(s: String): Unit = {
    val result = swapsCount(s)
    println(result)
  }

}