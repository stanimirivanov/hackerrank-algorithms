package strings

/**
 * King Robert has 7 kingdoms under his rule. He finds out from a raven that the
 * Dothraki are soon going to wage a war against him. But, he knows the Dothraki
 * need to cross the Narrow River to enter his realm. There is only one bridge
 * that connects both banks of the river which is sealed by a huge door.
 *
 * The king wants to lock the door so that the Dothraki can't enter. But, to lock
 * the door he needs a key that is an anagram of a certain palindrome string.
 *
 * The king has a string composed of lowercase English letters. Help him figure out
 * if any anagram of the string can be a palindrome or not.
 *
 * @see http://www.hackerrank.com/challenges/game-of-thrones
 */
object GameOfThronesI {

  // count the number of character entries, true if odd is 0 or 1
  /**
   * A word can be rearranged to palindrome if all individual characters have
   * even count, except possibly for one
   */
  def canBeRearrangedToPalindrome(word: String) = {
    val charNumEntries = word.groupBy(w => w).map(t => (t._1, t._2.length))
    val nrOddEntries = charNumEntries.foldLeft(0) {
      case (acc, (k, v)) => acc + (if (v % 2 == 0) 0 else 1)
    }
    nrOddEntries <= 1
  }

  def main(args: Array[String]) {
    val lines = io.Source.stdin.getLines().toList
    printResult(lines.head)
  }

  def printResult(line: String): Unit = {
    val result = canBeRearrangedToPalindrome(line)
    println(if (result) "YES" else "NO")
  }

}