package strings

/**
 * Shashank likes strings in which consecutive characters are different.
 * For example, he likes ABABA, while he doesn't like ABAA.
 * Given a string containing characters A and B only, he wants to change
 * it into a string he likes.
 * To do this, he is allowed to delete the characters in the string.
 *
 * Your task is to find the minimum number of required deletions.
 *
 * @see http://www.hackerrank.com/challenges/alternating-characters
 */
object AlternatingCharacters {

  def stringReduction(s: String): Int =
    s.toList.sliding(2).foldLeft(0)((acc, xs) => acc + countEqualPairs(xs))

  def countEqualPairs(xs: List[Char]): Int =
    if (xs.toSet.size == 1) 1 else 0

  def main(args: Array[String]) {
    val lines = io.Source.stdin.getLines().toList
    lines.tail.foreach(line => printResult(line))
  }

  def printResult(s: String): Unit = {
    val result = stringReduction(s)
    println(result)
  }

}