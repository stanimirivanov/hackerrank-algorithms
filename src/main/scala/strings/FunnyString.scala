package strings

/**
 * Suppose you have a string S which has length N and is indexed from 0 to N?1.
 * String R is the reverse of the string S. The string S is funny if the
 * condition |S_i?S_i?1|=|R_i?R_i?1| is true for every i from 1 to N?1.
 *
 * (Note: Given a string str, str_i denotes the ascii value of the i-th character
 * (0-indexed) of str. |x| denotes the absolute value of an integer x)
 *
 * @see https://www.hackerrank.com/challenges/funny-string
 */
object FunnyString {
  
  def isFunny(s: List[Char]): Boolean = {
    val r = s.reverse
    (s.sliding(2) zip r.sliding(2)) forall {
      case (sWin, rWin) =>
        Math.abs(sWin(1).toInt - sWin(0).toInt) == Math.abs(rWin(1).toInt - rWin(0).toInt)
    }
  }

  def main(args: Array[String]) {
    val lines = io.Source.stdin.getLines().toList
    lines.tail.foreach(printResult(_))
  }

  def printResult(s: String): Unit = {
    val result = isFunny(s.toList)
    if (result) println("Funny") else println("Not Funny")
  }
  
}