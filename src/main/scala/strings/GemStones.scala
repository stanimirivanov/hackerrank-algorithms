package strings

/**
 * John has discovered various rocks. Each rock is composed of various
 * elements, and each element is represented by a lowercase latin
 * letter from 'a' to 'z'. An element can be present multiple times in
 * a rock. An element is called a 'gem-element' if it occurs at least
 * once in each of the rocks.
 *
 * Given the list of N rocks with their compositions, display the number
 * of gem-elements that exist in those rocks.
 *
 * @see https://www.hackerrank.com/challenges/gem-stones
 */
object GemStones {

    def gemElementss(xs: List[String]): Set[Char] = {
        val stones = xs.map(_.toList.toSet)
        val masterStone = ('a' to 'z').toSet
        stones.foldLeft(masterStone)(intersect)
    }
    
    def intersect(a: Set[Char], b: Set[Char]): Set[Char] = a.intersect(b)
    
    def main(args: Array[String]) {
        val lines = io.Source.stdin.getLines().toList
        printResult(lines.tail)
    }

    def printResult(xs: List[String]): Unit = {
        val result = gemElementss(xs)
        println(result.size)
    }

}