package strings

import scala.languageFeature.postfixOps

/**
 * Now that the king knows how to find out whether a given word
 * has an anagram which is a palindrome or not, he encounters
 * another challenge. He realizes that there can be more than
 * one palindrome anagrams for a given word. Can you help him
 * find out how many palindrome anagrams are possible for a given
 * word ?
 *
 * The king has many words. For each given word, he needs to find
 * out the number of palindrome anagrams of the string. As the
 * number of anagrams can be large, the king needs the number of
 * anagrams % (10^9+ 7).
 *
 * @see https://www.hackerrank.com/challenges/game-of-throne-ii
 */
object GameOfThronesII {
  
  import scala.language.postfixOps

  def rangeProduct(n1: Long, n2: Long): BigInt = n2 - n1 match {
    case 0 => BigInt(n1)
    case 1 => BigInt(n1 * n2)
    case 2 => BigInt(n1 * (n1 + 1)) * n2
    case 3 => BigInt(n1 * (n1 + 1)) * ((n2 - 1) * n2)
    case _ =>
      val nm = (n1 + n2) >> 1
      rangeProduct(n1, nm) * rangeProduct(nm + 1, n2)
  }

  implicit class Factorial(val n: Int) extends AnyVal {
    def ! : BigInt = {
      rangeProduct(1, n)
    }
  }

  // @see http://en.wikipedia.org/wiki/Permutation#In_combinatorics (Permutations of multisets)
  def anagramCount(word: String): BigInt = {
    val frequiencesMap = word.groupBy(w => w).map(t => (t._1, t._2.length))
    val letters = word.distinct filter { c => frequiencesMap.getOrElse(c, 0) % 2 == 0 }
    val frequiences = letters map { c => math.floor(frequiencesMap.getOrElse(c, 0) / 2).toInt }
    val n = math.floor(word.size / 2).toInt
    val denum = frequiences.foldLeft(BigInt(1))((acc, f) => acc * (f!))
    (n!) / denum
  }

  def main(args: Array[String]) {
    val lines = io.Source.stdin.getLines().toList
    printResult(lines.head)
  }

  def printResult(line: String): Unit = {
    val m = 1000000007
    val result = anagramCount(line)
    println(result % m)
  }

}