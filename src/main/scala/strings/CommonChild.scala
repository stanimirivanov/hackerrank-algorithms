package strings

/**
 * Given two strings a and b of equal length, what's the longest
 * string (S) that can be constructed such that S is a child of
 * both a and b.
 *
 * A string x is said to be a child of a string y, if x can be
 * formed by deleting 0 or more characters from y
 *
 * @see https://www.hackerrank.com/challenges/common-child
 */
object CommonChild {

  /**
   * This is the longest common subsequence problem without backtracking the subsequence. 
   */
  def lcs(a: String, b: String): Int = {
    val lcsTable = buildLcsTable(a, b)
    lcsTable(a.size)(b.size)
  }

  def buildLcsTable(a: String, b: String): Array[Array[Int]] = {
    val as = a.toCharArray
    val bs = b.toCharArray
    val lsc = Array.ofDim[Int](as.size + 1, bs.size + 1)
    for (i <- 0 until as.size) {
      for (j <- 0 until bs.size) {
        if (as(i) == bs(j)) lsc(i + 1)(j + 1) = lsc(i)(j) + 1
        else lsc(i + 1)(j + 1) = math.max(lsc(i + 1)(j), lsc(i)(j + 1))
      }
    }
    lsc
  }

  def main(args: Array[String]) {
    val lines = io.Source.stdin.getLines().toArray
    printResult(lines(0), lines(1))
  }

  def printResult(a: String, b: String): Unit = {
    val result = lcs(a, b)
    println(result)
  }

}