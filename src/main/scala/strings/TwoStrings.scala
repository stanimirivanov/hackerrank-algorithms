package strings

/**
 * You are given two strings, A and B. Find if there is a substring that appears
 * in both A and B.
 *
 * @see https://www.hackerrank.com/challenges/two-strings
 */
object TwoStrings {
  
  def existSubstring(as: List[Char], bs: List[Char]): Option[List[Char]] = {
    val excluded = as diff bs
    val substring = as diff excluded
    println(s"as: $as bs: $bs substring: $substring")
    substring match {
      case Nil => None
      case _   => Some(substring)
    }
  }

  def main(args: Array[String]) {
    val lines = io.Source.stdin.getLines().toArray
    lines.tail.grouped(2) foreach { printResult(_) }
  }

  def printResult(lines: Array[String]): Unit = {
    val Array(a, b) = lines
    val result = existSubstring(a.toList, b.toList)
    result match {
      case None => println("NO")
      case _    => println("YES")
    }
  }
  
}