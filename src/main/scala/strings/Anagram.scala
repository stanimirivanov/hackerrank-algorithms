package strings

/**
 * Sid is obsessed about reading short stories. Being a CS student, he is
 * doing some interesting frequency analysis with the books. He has two
 * short story books. He chooses strings having length a from the first
 * book and strings having length b from the second one. The strings are
 * such that the difference of length is <= 1
 *
 * i.e.
 *
 * |a-b| <= 1, where |x| represents the absolute value of x.
 *
 * He believes that both the strings should be anagrams based on his experiment.
 * Your challenge is to help him find the minimum number of characters of the
 * first string he needs to change to enable him to make it an anagram of the
 * second string. He can neither add a character nor delete a character from
 * the first string. Only replacement of the original characters with the new
 * ones is allowed.
 *
 * @see https://www.hackerrank.com/challenges/anagram
 */
object Anagram {

  def minimumSteps(ab: String): Int =
    if (ab.size % 2 == 1) -1
    else {
      val cs = ab.toList
      val i = (cs.size / 2).toInt
      val first = cs.take(i).sorted
      val last = cs.reverse.take(i).sorted
      (first diff last).size
    }

  def main(args: Array[String]) {
    val lines = io.Source.stdin.getLines().toList
    lines.tail.foreach(s => printResult(s))
  }

  def printResult(s: String): Unit = {
    val result = minimumSteps(s)
    println(result)
  }

}