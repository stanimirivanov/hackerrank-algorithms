package strings

/**
 * You are given a string of lowercase letters. Your task is to
 * figure out the index of the character on whose removal will
 * make the string a palindrome. There will always be a valid solution.
 *
 * In case string is already palindrome, then -1 is also a valid answer
 * along with possible indices.
 *
 * @see https://www.hackerrank.com/challenges/palindrome-index
 */
object PalindromeIndex {

  def palindromeIndex(word: String): Int = {
    @annotation.tailrec
    def iter(cs: Vector[Char], i: Int, j: Int): Int = {
      if (j <= i) -1
      else if (cs(i) != cs(j)) index(cs, i, j)
      else iter(cs, i + 1, j - 1)
    }
    iter(word.toVector, 0, word.size - 1)
  }

  def index(cs: Vector[Char], i: Int, j: Int): Int =
    if (isPalindrome(cs.slice(i + 1, j + 1))) i else j

  def isPalindrome(cs: Vector[Char]): Boolean =
    cs.reverse.sameElements(cs)

  def main(args: Array[String]) {
    val lines = io.Source.stdin.getLines().toList
    lines.tail.foreach(s => printResult(s))
  }

  def printResult(s: String): Unit = {
    val result = palindromeIndex(s)
    println(result)
  }

}