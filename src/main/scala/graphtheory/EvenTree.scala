package graphtheory

import scala.collection.mutable.ListBuffer

/**
 * You are given a tree (a simple connected graph with no cycles).
 * You have to remove as many edges from the tree as possible to
 * obtain a forest with the condition that : Each connected component
 * of the forest should contain an even number of vertices.
 *
 * To accomplish this, you will remove some edges from the tree.
 * Find out the number of removed edges.
 *
 * @see http://www.hackerrank.com/challenges/even-tree
 * @see https://github.com/vkostyukov/scalacaster/blob/master/src/graph/Graph.scala
 */
object EvenTree {

  case class Edge[E, N](source: Graph[E, N], target: Graph[E, N], value: E)

  class Graph[E, N](var value: N = null.asInstanceOf[N]) {

    import scala.collection.immutable.Queue

    var inEdges: List[Edge[E, N]] = Nil
    var outEdges: List[Edge[E, N]] = Nil

    def succs: List[Graph[E, N]] = outEdges.map(_.target)

    def preds: List[Graph[E, N]] = inEdges.map(_.source)

    def connect(from: N, via: E, to: N): (Graph[E, N], Graph[E, N]) = {
      val fromGraph: Graph[E, N] = if (value == null) { value = from; this } else hop(from) match {
        case Some(g) => g
        case None    => Graph.one(from)
      }

      val toGraph: Graph[E, N] = hop(to) match {
        case Some(g) => g
        case None    => Graph.one(to)
      }

      fromGraph.outEdges = new Edge(fromGraph, toGraph, via) :: fromGraph.outEdges
      toGraph.inEdges = new Edge(fromGraph, toGraph, via) :: toGraph.inEdges

      (fromGraph, toGraph)
    }

    def hop(n: N): Option[Graph[E, N]] = graphsByDepth.find(_.value == n)

    def nodesByDepth: List[N] = graphsByDepth.map(_.value)

    def nodesByBreadth: List[N] = graphsByBreadth.map(_.value)

    def graphsByDepth: List[Graph[E, N]] = {
      def loop(g: Graph[E, N], s: Set[Graph[E, N]]): Set[Graph[E, N]] =
        if (!s(g)) {
          val ss = g.succs.foldLeft(s + g)((acc, gg) => loop(gg, acc))
          g.preds.foldLeft(ss)((acc, gg) => loop(gg, acc))
        } else s

      loop(this, Set()).toList
    }

    def graphsByBreadth: List[Graph[E, N]] = {
      def loop(q: Queue[Graph[E, N]], s: Set[Graph[E, N]]): Set[Graph[E, N]] =
        if (!q.isEmpty && !s(q.head)) {
          val qq = q.head.succs.foldLeft(q.tail)((acc, gg) => acc :+ gg)
          loop(q.head.preds.foldLeft(qq)((acc, gg) => acc :+ gg), s + q.head)
        } else s

      loop(Queue(this), Set()).toList
    }

    def subTree: List[Graph[E, N]] = {
      def loop(g: Graph[E, N], s: Set[Graph[E, N]]): Set[Graph[E, N]] =
        if (!s(g)) g.succs.foldLeft(s + g)((acc, gg) => loop(gg, acc))
        else s

      loop(this, Set()).toList
    }

    def size: Int = graphsByDepth.size

    override def equals(o: Any): Boolean = o match {
      case g: Graph[_, _] => g.value == value
      case _              => false
    }

    override def toString: String = "Graph(" + value + ")"
  }

  object Graph {
    def apply[E, N](tuples: (N, E, N)*): Graph[E, N] = {
      var g: Graph[E, N] = Graph.empty
      for ((from, via, to) <- tuples) {
        g.connect(from, via, to)
      }
      g
    }

    def one[E, N](n: N): Graph[E, N] = new Graph(n)

    def empty[E, N]: Graph[E, N] = new Graph
  }

  /**
   * Greedy approach.
   */
  def evenTrees(g: Graph[Int, String]): Int = {
    def loop(trees: List[Graph[Int, String]], acc: List[Int]): List[Int] = {
      if (trees.isEmpty) acc
      else {
        val subTrees = trees.flatMap(_.succs)
        val newAcc = trees.foldLeft(acc)((acc2, t) => t.subTree.size :: acc2)
        loop(subTrees, newAcc)
      }
    }

    val sizes = loop(g.succs, Nil)
    val evens = sizes filter { _ % 2 == 0 }
    evens.size
  }

  def main(args: Array[String]) {
    val lines = io.Source.stdin.getLines().toList
    printResult(lines.tail)
  }

  def buildGraph(lines: List[String]): Graph[Int, String] = {
    val g: Graph[Int, String] = Graph.empty
    lines foreach { line =>
      val nodes = line.split(" ").toArray
      g.connect(nodes(1), 0, nodes(0))
    }
    g
  }

  def printResult(lines: List[String]): Unit = {
    val g = buildGraph(lines)
    val result = evenTrees(g)
    println(result)
  }

}