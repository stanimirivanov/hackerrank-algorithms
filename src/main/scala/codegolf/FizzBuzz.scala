package codegolf

/**
 * Write a short program that prints (to STDOUT) the numbers from 1 to 100.
 * But for multiples of three print "Fizz" instead of the number and for
 * the multiples of five print "Buzz". For numbers which are multiples of
 * both three and five print "FizzBuzz".
 *
 * The goal is to write the shortest code possible.
 *
 * Scoring: Your score is (200 - number of characters in your source code)/10
 *
 * @see https://www.hackerrank.com/challenges/fizzbuzz
 */
object FizzBuzz extends App{for(i<-1 to'd')println(Set("Fizz"*(1-i%3)+"Buzz"*(1-i%5),i)-""head)}