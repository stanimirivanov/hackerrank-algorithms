package codegolf

/**
 * Given a partially filled 9 x 9 sudoku board, you need to output a solution to it.
 *
 * Scoring: ratio (4000 - characters in the source code)/40
 *
 * @see https://www.hackerrank.com/challenges/leibniz
 */
object Sudoku extends App{
type B=Vector[Vector[Int]]
def s(b:B,x:Int):Option[B]=(x%9, x/9)match{
case (r,9)=>Some(b)
case (r,c)if b(r)(c)>0=>s(b,x+1)
case (r,c)=>1 to 9 diff (b.indices.flatMap(i=>Vector(b(r)(i),b(i)(c),b(3*(r/3)+i/3)(3*(c/3)+i%3)))) collectFirst Function.unlift(x=>s(b.updated(r,b(r).updated(c,x)),x+1))
}
io.Source.stdin.getLines
.drop(1)
.grouped(9)
.foreach(n=>println(s(n.map(_.split(" ").map(_.toInt).toVector).toVector, 0).get map(_ mkString " ") mkString "\n"))
}