package codegolf

/**
 * The Sine, Cosine of x can be computed as follows.
 *
 * sin(x) = x - x3/3! + x5/5! - x7/7! + x9/9! ....
 * cos(x) = 1 - x2/2! + x4/4! - x6/6! + x8/8! ....
 *
 * Your task is to compute the Sine and Cosine for given values
 * of x (where x is in radians) using the above series up to 5 terms.
 *
 * Scoring: Score = maxScore* (400 - S)/400
 * S = min(Number of characters in source code,399)
 *
 * @see https://www.hackerrank.com/challenges/trignometric-ratios
 */
object TrigonometricRatios extends App{io.Source.stdin.getLines.drop(1).map(_.toDouble).foreach(x=>println(x-x*x*x/6+x*x*x*x*x/120-math.pow(x,7)/5040+math.pow(x,9)/362880+"\n"+(1-x*x/2+x*x*x*x/24-math.pow(x,6)/720+math.pow(x,8)/40320)))}