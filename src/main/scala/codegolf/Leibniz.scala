package codegolf

/**
 * In Calculus, the Leibniz formula for ? is given by:
 *
 * sin(x) = x - 1/3 + 1/5 - 1/7 + 1/9 .... = pi/4
 *
 * You will be given an integer n. Your task is to print the summation of the
 * Leibniz formula up to the nth term of the series correct to 15 decimal
 * places.
 *
 * Scoring: A correct submission with a source code of X characters will
 * receive the following score:
 * Score = maxScore * (300 - X)/300
 * Any correct code longer than 300 characters will receive a score of maxScore * 0.001.
 *
 * @see https://www.hackerrank.com/challenges/leibniz
 */
object Leibniz extends App{io.Source.stdin.getLines.drop(1).map(_.toInt).foreach(n=>println((0 to n-1).map(i=>math.pow(-1,i)/(2*i+1)).sum))}