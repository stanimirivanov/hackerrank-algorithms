package gametheory

/**
 * Dexter and Debra are playing a game. They have N containers each having one
 * or more chocolates. Containers are numbered from 1 to N, where i-th container
 * has A[i] number of chocolates.
 *
 * The game goes like this. First player will choose a container and take one or
 * more chocolates from it. Then, second player will choose a non-empty
 * container and take one or more chocolates from it. And then they alternate
 * turns. This process will continue, until one of the players is not able to
 * take any chocolates (because no chocolates are left). One who is not able to
 * take any chocolates loses the game. Note that player can choose only
 * non-empty container.
 *
 * The game between Dexter and Debra has just started, and Dexter has got the
 * first Chance. He wants to know the number of ways to make a first move such
 * that under optimal play, the first player always wins.
 *
 * @see https://www.hackerrank.com/challenges/chocolate-in-box
 */
object ChocolateInBox {

  def firstWinningMoves(as: List[Int]): Int = {
    val xorAll = as.foldLeft(0)(_ ^ _)
    as.foldLeft(0)((acc, x) => if ((xorAll ^ x) < x) acc + 1 else acc)
  }

  def main(args: Array[String]) {
    val lines = io.Source.stdin.getLines().toList
    printResult(lines)
  }

  def printResult(lines: List[String]): Unit = {
    val xs = lines.tail.head.split(" ").toList.map(_.toInt)
    val result = firstWinningMoves(xs)
    println(result)
  }

}