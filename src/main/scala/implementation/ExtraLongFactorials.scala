package implementation

/**
 * You are given an integer N. Print the factorial of this number.
 *
 * N! = N.(N?1).(N?2)...3.2.1
 *
 * Note: Factorials of N>20 can't be stored even in a 64?bit long long
 * variable. Big integers must be used for such calculations. Languages
 * like Java, Python, Ruby etc. can handle big integers but we need to
 * write additional code in C/C++ to handle such large values.
 *
 * We recommend solving this challenge using BigIntegers.
 *
 * @see https://www.hackerrank.com/challenges/extra-long-factorials
 */
object ExtraLongFactorials {

  def factorial(n: BigInt): BigInt =
    (BigInt(1) to n).par.fold(BigInt(1))(_ * _)

  def main(args: Array[String]) {
    val lines = io.Source.stdin.getLines().toList
    printResult(lines.head)
  }

  def printResult(s: String): Unit = {
    val n = BigInt(s)
    val result = factorial(n)
    println(result)
  }

}