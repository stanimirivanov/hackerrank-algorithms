package implementation

import scala.collection.immutable.IndexedSeq

/**
 * Given a 2D array of digits, try to find the location of a given 2D pattern of
 * digits. For example, consider the following 2D matrix:
 *
 * 1234567890
 * 0987654321
 * 1111111111
 * 1111111111
 * 2222222222
 *
 * Assume we need to look for the following 2D pattern:
 *
 * 876543
 * 111111
 * 111111
 *
 * If we scan through the original array, we observe that the 2D pattern begins
 * at the second row and the third column of the larger grid (the 8 in the second
 * row and third column of the larger grid is the top-left corner of the pattern
 * we are searching for).
 *
 * So, a 2D pattern of P digits is said to be present in a larger grid G, if the
 * latter contains a contiguous, rectangular 2D grid of digits matching with the
 * pattern P, similar to the example shown above.
 *
 * @see https://www.hackerrank.com/challenges/the-grid-search
 */
object GridSearch {

  class Matrix(val elements: Vector[String]) {
    def nRows: Int = elements.length

    def nCols: Int = if (elements.isEmpty) 0 else elements.head.length

    def row(idx: Int): String = elements(idx)

    override def toString() = elements.mkString(" ")
  }

  object Matrix {
    def apply(lines: Vector[String]): Matrix = {
      new Matrix(lines)
    }
  }

  def patternMatch(matrix: Matrix, pattern: Matrix): Boolean = {
    def loop(rows: Vector[String], pattern: Matrix): Boolean = {
      if (rows.size < pattern.nRows) false
      else {
        val firstMatch = rows.head contains pattern.row(0)
        if (firstMatch) matchRows(rows, pattern) || loop(rows.tail, pattern)
        else loop(rows.tail, pattern)
      }
    }

    loop(matrix.elements, pattern)
  }

  def matchRows(rows: Vector[String], pattern: Matrix): Boolean = {
    val indexes = pattern.elements.zipWithIndex map {
      case (row, idx) => rows(idx).indexOfSlice(row)
    }

    val distinctIndexes = indexes.distinct
    distinctIndexes.size == 1 && distinctIndexes.head > -1
  }

  def main(args: Array[String]) {
    val lines = io.Source.stdin.getLines().toVector
    val parsedInput = parseInput(lines.tail)
    parsedInput.foreach(input => printResult(input))
  }

  def parseInput(lines: Vector[String]): List[(Matrix, Matrix)] = {
    def loop(lines: Vector[String], result: List[(Matrix, Matrix)]): List[(Matrix, Matrix)] = {
      if (lines.isEmpty) result
      else {
        val Array(mRows, _) = lines(0).split(" ").map(_.toInt)
        val Array(pRows, _) = lines(mRows + 1).split(" ").map(_.toInt)
        val matrix = Matrix(lines.slice(1, mRows + 1))
        val pattern = Matrix(lines.slice(mRows + 2, pRows + mRows + 2))
        loop(lines.drop(pRows + mRows + 2), (matrix, pattern) :: result)
      }
    }
    loop(lines, Nil).reverse
  }

  def printResult(input: (Matrix, Matrix)): Unit = {
    val result = patternMatch(input._1, input._2)
    result match {
      case true => println("YES")
      case false => println("NO")
    }
  }

}