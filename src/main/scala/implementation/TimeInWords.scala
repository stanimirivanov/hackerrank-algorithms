package implementation

/**
 * Given the time in numerals we may convert it into words, as shown below:
 *
 * 5:00 => five o' clock
 * 5:01 => one minute past five
 * 5:10 => ten minutes past five
 * 5:30 => half past five
 * 5:40 => twenty minutes to six
 * 5:45 => quarter to six
 * 5:47 => thirteen minutes to six
 * 5:28 => twenty eight minutes past five
 *
 * Write a program which prints the time in words for the input given in the format
 * mentioned above.
 *
 * @see https://www.hackerrank.com/challenges/the-time-in-words
 */
object TimeInWords {

  class TimeWords(hours: Int, minutes: Int) {

    val minutesToWords = Map(1 -> "one minute",
      2 -> "two minutes",
      3 -> "three minutes",
      4 -> "four minutes",
      5 -> "five minutes",
      6 -> "six minutes",
      7 -> "seven minutes",
      8 -> "eight minutes",
      9 -> "nine minutes",
      10 -> "ten minutes",
      11 -> "eleven minutes",
      12 -> "twelve minutes",
      13 -> "thirteen minutes",
      14 -> "fourteen minutes",
      15 -> "quarter",
      16 -> "sixteen minutes",
      17 -> "seventeen minutes",
      18 -> "eighteen minutes",
      19 -> "nineteen minutes",
      20 -> "twenty minutes",
      21 -> "twenty one minutes",
      22 -> "twenty two minutes",
      23 -> "twenty three minutes",
      24 -> "twenty four minutes",
      25 -> "twenty five minutes",
      26 -> "twenty six minutes",
      27 -> "twenty seven minutes",
      28 -> "twenty eight minutes",
      29 -> "twenty nine minutes",
      30 -> "half")

    val hoursToWords = Map(1 -> "one",
      2 -> "two",
      3 -> "three",
      4 -> "four",
      5 -> "five",
      6 -> "six",
      7 -> "seven",
      8 -> "eight",
      9 -> "nine",
      10 -> "ten",
      11 -> "eleven",
      12 -> "twelve",
      13 -> "one")

    override def toString() = {
      if (minutes == 0) hoursToWords(hours) + " o' clock"
      else if (minutes <= 30) minutesToWords(minutes) + " past " + hoursToWords(hours)
      else minutesToWords(60 - minutes) + " to " + hoursToWords(hours + 1)
    }
  }

  def main(args: Array[String]) {
    val lines = io.Source.stdin.getLines().toArray
    printResult(lines)
  }

  def printResult(lines: Array[String]): Unit = {
    val hours = lines(0).toInt
    val minutes = lines(1).toInt
    val result = new TimeWords(hours, minutes)
    println(result)
  }

}