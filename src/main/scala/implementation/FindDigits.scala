package implementation

/**
 * You are given an integer N.
 * Find the digits in this number that exactly divide N (division that leaves 0
 * as remainder) and display their count.
 * For N=24, there are 2 digits − 2 & 4.
 * Both of these digits exactly divide 24. So our answer is 2.
 *
 * @see http://www.hackerrank.com/challenges/find-digits
 */
object FindDigits {

  def countDivisors(n: Int): Int = {
    val digits = n.toString.map(_.asDigit)
    digits.filterNot(_ == 0).filter(n % _ == 0).size
  }

  def main(args: Array[String]) {
    val lines = io.Source.stdin.getLines().drop(1).toList
    lines.foreach(line => printResult(line))
  }

  def printResult(s: String): Unit = {
    val result = countDivisors(s.toInt)
    println(result)
  }

}