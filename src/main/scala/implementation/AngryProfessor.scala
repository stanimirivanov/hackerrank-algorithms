package implementation

/**
 * The professor is conducting a course on Discrete Mathematics to a class of N
 * students. He is angry at the lack of their discipline, and he decides to
 * cancel the class if there are fewer than K students present after the class
 * starts.
 *
 * Given the arrival time of each student, your task is to find out if the class
 * gets cancelled or not.
 *
 * @see https://www.hackerrank.com/challenges/angry-professor
 */
object AngryProfessor {

  def classCanceled(k: Int, as: List[Int]): Boolean = {
    val onTime = as count { _ <= 0 }
    onTime < k
  }

  def main(args: Array[String]) {
    val lines = io.Source.stdin.getLines().toList
    lines.tail.grouped(2).foreach(ls => printResult(ls))
  }

  def printResult(lines: List[String]): Unit = {
    val Array(n, k) = lines.head.split(" ").map(_.toInt)
    val as = lines.tail.head.split(" ").map(_.toInt).toList
    val result = classCanceled(k, as)
    result match {
      case true  => println("YES")
      case false => println("NO")
    }
  }

}