package implementation

import scala.collection.immutable.IndexedSeq

/**
 * You are given a 2D matrix, a, of dimension MxN and a positive integer R. You have
 * to rotate the matrix R times and print the resultant matrix. Rotation should be
 * in anti-clockwise direction.
 *
 * It is guaranteed that the minimum of M and N will be even.
 *
 * @see https://www.hackerrank.com/challenges/matrix-rotation-algo
 */
object MatrixRotation {

  class Matrix(elements: Vector[Vector[Int]]) {
    def nRows: Int = elements.length
    def nCols: Int = if (elements.isEmpty) 0 else elements.head.length

    override def toString() = elements.mkString(" ")
  }

  object Matrix {
    def apply(lines: List[String]): Matrix = {
      val rows: Vector[Vector[Int]] = (lines map { line: String =>
        (line.split(" ") map { s: String => s.toInt }).toVector
      }).toVector
      new Matrix(rows)
    }
  }

  def rotate(m: Matrix): Matrix = m

  def main(args: Array[String]) {
    val lines = io.Source.stdin.getLines().toList
    printResult(lines)
  }

  def printResult(lines: List[String]): Unit = {
    val Array(_, _, rotations) = lines.head.split(" ").map(_.toInt)
    val m = Matrix(lines.tail)
    val result = rotate(m)
    println(result)
  }

}