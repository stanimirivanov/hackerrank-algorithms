package implementation

/**
 * Watson gives to Sherlock an array: A1, A2, ..., AN. He also gives to Sherlock two
 * other arrays: B1, B2, ..., BM and C1, C2, ..., CM.
 * Then Watson asks Sherlock to perform the following program:
 *
 * for i = 1 to M do
 *    for j = 1 to N do
 *      if j % B[i] == 0 then
 *        A[j] = A[j] * C[i]
 *      endif
 *    end do
 * end do
 *
 * Can you help Sherlock and tell him the resulting array A?
 * You should print all the array elements modulo (10^9+7).
 *
 * @see https://www.hackerrank.com/challenges/sherlock-and-queries/
 */
object SherlockAndQueries {

  import scala.collection.mutable.Buffer

  def decode(as: Buffer[Long], bs: List[Int], cs: List[Int]): Buffer[Long] = {
    val n = as.size
    val mod = 1000000007

    val counts = (bs zip cs).foldLeft(Map[Int, Int]())((acc, t) => {
      t match {
        case (b, c) =>
          val count = acc.get(b) match {
            case Some(x) => Map(b -> ((x.toLong * c) % mod).toInt)
            case None    => Map(b -> c)
          }
          acc ++ count
      }
    })

    counts map {
      case (divisor, multiplier) =>
        divisor - 1 until n by divisor foreach { j =>
          as(j) = (as(j) * multiplier) % mod
        }
    }
    as
  }

  def main(args: Array[String]) {
    val lines = io.Source.stdin.getLines().toArray
    printResult(lines)
  }

  def printResult(lines: Array[String]): Unit = {
    val a = lines(1).split(" ").map(_.toLong).toBuffer
    val b = lines(2).split(" ").map(_.toInt).toList
    val c = lines(3).split(" ").map(_.toInt).toList
    val result = decode(a, b, c)
    println(result.mkString(" "))
  }
}