package implementation

/**
 * Little Bob loves chocolates, and goes to a store with $N in his pocket.
 * The price of each chocolate is $C. The store offers a discount: for
 * every M wrappers he gives to the store, he gets one chocolate for free.
 * How many chocolates does Bob get to eat?
 *
 * @see http://www.hackerrank.com/challenges/chocolate-feast
 */
object ChocolateFeast {

  def chocolateCount(money: Int, price: Int, discountRate: Int): Int = {
    val chocolates = money / price
    val extras = exchangeWrappers(chocolates, discountRate)
    chocolates + extras
  }

  def exchangeWrappers(wrappers: Int, discountRate: Int): Int = {
    if (wrappers < discountRate) 0
    else {
      val chocolates = wrappers / discountRate
      val moneyLeft = wrappers % discountRate
      if (moneyLeft == 0 && chocolates < discountRate) chocolates
      else chocolates + exchangeWrappers(chocolates + moneyLeft, discountRate)
    }
  }

  def main(args: Array[String]) {
    val lines = io.Source.stdin.getLines().toList
    lines.tail.foreach(s => printResult(s))
  }

  def printResult(s: String): Unit = {
    val input = s.split(" ").map(_.toInt)
    val result = chocolateCount(input(0), input(1), input(2))
    println(result)
  }

}