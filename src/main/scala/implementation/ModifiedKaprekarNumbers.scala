package implementation

/**
 * A modified Kaprekar number is a positive whole number n with d digits, such
 * that when we split its square into two pieces - a right hand piece r with d
 * digits and a left hand piece l that contains the remaining d or d?1 digits,
 * the sum of the pieces is equal to the original number (i.e. l + r = n).
 *
 * Alternatively, a modified Kaprekar number is a positive whole number n with
 * 2d digits (if its number of digits is even) or 2d + 1 digits (if its number
 * of digits is odd), such that when we split its square into two pieces, a
 * right hand piece r containing d or d + 1 digits, and a left piece l
 * containing the remaining d digits, the sum of the two pieces is equal to the
 * original number.
 *
 * Note: r may have leading zeros.
 *
 * Here's an alternative explanation from Wikipedia: In mathematics, a Kaprekar
 * number for a given base is a non-negative integer, the representation of
 * whose square in that base can be split into two parts that add up to the
 * original number again. For instance, 45 is a Kaprekar number, because 45^2 =
 * 2025 and 20+25 = 45.
 *
 * The Task
 * You are given the two positive integers p and q, where p is lower than q. Write
 * a program to determine how many Kaprekar numbers are there in the range between
 * p and q (both inclusive) and display them all.
 *
 * @see https://www.hackerrank.com/challenges/kaprekar-numbers
 */
object ModifiedKaprekarNumbers {

  def isKaprekarNumber(num: Long): Boolean = {
    val s = (num * num).toString
    val d = s.length/2
    num match {
      case _ if (num < 1)       => false
      case _ if (s.length == 1) => s.toLong == num
      case _                    =>
        val left = s.take(d).toLong
        val right = s.drop(d).toLong
        left + right == num
    }
  }

  def kaprekarNumbers(p: Long, q: Long): Seq[Long] =
    (p to q) filter { isKaprekarNumber }

  def main(args: Array[String]) {
    val lines = io.Source.stdin.getLines().toArray
    printResult(lines)
  }

  def printResult(lines: Array[String]): Unit = {
    val p = lines(0).toLong
    val q = lines(1).toLong
    val result = kaprekarNumbers(p, q)
    result match {
      case Nil => println("INVALID RANGE")
      case _   => println(result.mkString(" "))
    }
  }

}