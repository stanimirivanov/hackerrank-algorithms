package implementation

/**
 * You are given N sticks, where each stick is of positive integral length. A cut
 * operation is performed on the sticks such that all of them are reduced by the
 * length of the smallest stick.
 *
 * Suppose we have 6 sticks of length
 *
 * 5 4 4 2 2 8
 *
 * then in one cut operation we make a cut of length 2 from each of the 6 sticks. For
 * next cut operation 4 sticks are left (of non-zero length), whose length are
 *
 * 3 2 2 6
 *
 * Above step is repeated till no sticks are left.
 *
 * Given length of N sticks, print the number of sticks that are cut in subsequent cut
 * operations.
 *
 * @see https://www.hackerrank.com/challenges/cut-the-sticks
 */
object CutTheSticks {

  def cut(sticks: List[Int]): List[Int] = {
    def loop(sticks: List[Int], sticksCut: List[Int]): List[Int] =
      if (sticks.isEmpty) sticksCut
      else {
        val smallest = sticks.head
        val cuts = sticks.tail.map(_ - smallest).filterNot(_ == 0)
        loop(cuts, sticks.size :: sticksCut)
      }

    loop(sticks.sorted, Nil).reverse
  }

  def main(args: Array[String]) {
    val lines = io.Source.stdin.getLines().toList
    printResult(lines.tail.head)
  }

  def printResult(s: String): Unit = {
    val sticks = s.split(" ").map(_.toInt).toList
    val result = cut(sticks)
    result foreach { c =>
      println(c)
    }
  }
}