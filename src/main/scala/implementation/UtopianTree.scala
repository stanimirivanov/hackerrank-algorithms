package implementation

/**
 * The Utopian tree goes through 2 cycles of growth every year.
 * The first growth cycle occurs during the spring, when it doubles in height.
 * The second growth cycle occurs during the summer, when its height increases by 1 meter.
 * Now, a new Utopian tree sapling is planted at the onset of the spring.
 * Its height is 1 meter. Can you find the height of the tree after N growth cycles?
 *
 * @see http://www.hackerrank.com/challenges/utopian-tree
 */
object UtopianTree {

  def utopianHeight(cycles: Int): Int =
    (1 to cycles).foldLeft(1)((height, cycle) => grow(height, cycle))

  def grow(height: Int, cycle: Int): Int =
    if (cycle % 2 == 0) height + 1 else 2 * height

  def main(args: Array[String]) {
    val lines = io.Source.stdin.getLines().toList
    lines.tail.foreach(s => printResult(s))
  }

  def printResult(s: String): Unit = {
    val result = utopianHeight(s.toInt)
    println(result)
  }

}