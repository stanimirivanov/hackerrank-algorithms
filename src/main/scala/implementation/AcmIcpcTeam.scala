package implementation

/**
 * You are given a list of N people who are attending ACM-ICPC World Finals.
 * Each of them are either well versed in a topic or they are not. Find out
 * the maximum number of topics a 2-person team can know. And also find out
 * how many teams can know that maximum number of topics.
 *
 * @see http://www.hackerrank.com/challenges/acm-icpc-team
 */
object AcmIcpcTeam {

  def maxTopicsNumber(people: List[BigInt]): Option[List[Int]] = {
    val teams = people.combinations(2).map(teamUp(_)).map(_.bitCount).toList.sortWith(_ > _)
    val topics = teams.head
    topics match {
      case 0 => None
      case _ => Some(teams.filter(_ == topics))
    }
  }

  def teamUp(people: List[BigInt]): BigInt =
    people.head | people.tail.head

  def main(args: Array[String]) {
    val lines = io.Source.stdin.getLines().toList
    printResult(lines)
  }

  def printResult(lines: List[String]): Unit = {
    val people = lines.tail.map(p => BigInt(p, 2))
    val result = maxTopicsNumber(people)
    result match {
      case Some(topics) => {
        println(topics.head)
        println(topics.size)
      }
      case None => {
        println(0)
        println(0)
      }
    }
  }

}
