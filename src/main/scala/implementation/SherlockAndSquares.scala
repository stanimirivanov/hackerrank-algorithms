package implementation

/**
 * Watson gives two integers (A and B) to Sherlock and asks if he can count the
 * number of square integers between A and B (both inclusive).
 *
 * Note: A square integer is an integer which is the square of any integer. For
 * example, 1, 4, 9, and 16 are some of the square integers as they are squares
 * of 1, 2, 3, and 4, respectively.
 *
 * @see https://www.hackerrank.com/challenges/sherlock-and-squares
 */
object SherlockAndSquares {

  def countSquares(a: Int, b: Int): Int =
    (math.floor(math.sqrt(b)) - math.floor(math.sqrt(a-1))).toInt

  def main(args: Array[String]) {
    val lines = io.Source.stdin.getLines().toList
    lines.tail.foreach(s => printResult(s))
  }

  def printResult(s: String): Unit = {
    val input = s.split(" ").map(_.toInt)
    val result = countSquares(input(0), input(1))
    println(result)
  }
}