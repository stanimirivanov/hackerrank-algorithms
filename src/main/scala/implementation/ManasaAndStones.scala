package implementation

/**
 * Manasa is out on a hike with friends. She finds a trail of stones with numbers on them.
 * She starts following the trail and notices that two consecutive stones have a difference
 * of either a or b. Legend has it that there is a treasure trove at the end of the trail and
 * if Manasa can guess the value of the last stone, the treasure would be hers. Given that the
 * number on the first stone was 0, find all the possible values for the number on the last stone.
 *
 * Note: The numbers on the stones are in increasing order
 *
 * @see http://www.hackerrank.com/challenges/manasa-and-stones
 */
object ManasaAndStones {

  /**
   * Computes all values of x*a + y*b where x+y=(n-1).
   */
  def possibleValues(n: Int, a: Int, b: Int): List[Int] =
    (0 until n).map(i => i * a + (n - 1 - i) * b).distinct.sorted.toList

  def main(args: Array[String]) {
    val lines = io.Source.stdin.getLines().toList
    lines.tail.map(_.toInt).grouped(3).foreach(xs => printResult(xs))
  }

  def printResult(xs: List[Int]): Unit = {
    val n = xs.head
    val a = xs.tail.head
    val b = xs.tail.tail.head
    val result = possibleValues(n, a, b)
    println(result.mkString(" "))
  }

}