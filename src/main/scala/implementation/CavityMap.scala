package implementation

/**
 * You are given a square n×n map. Each cell of the map has a value in it denoting the
 * depth of the appropriate area. We will call a cell of the map a cavity if and only
 * if this cell is not on the border of the map and each cell adjacent to it has
 * strictly smaller depth. Two cells are adjacent if they have a common side.
 *
 * You need to find all the cavities on the map and depict them with character X.
 *
 * @see https://www.hackerrank.com/challenges/cavity-map
 */
object CavityMap {
  
  def cavityMap(board: Vector[Vector[Char]]): Seq[Seq[Char]] = {
    val n = board.size - 1
    (0 to n) map { i =>
      (0 to n) map { j =>
        (i, j) match {
          case (0, _)             => board(0)(j)
          case (_, 0)             => board(i)(0)
          case (_, _) if (i == n) => board(n)(j)
          case (_, _) if (j == n) => board(i)(n)
          case (_, _) => {
            val depth = board(i)(j)
            if (depth > board(i - 1)(j)
              && depth > board(i + 1)(j)
              && depth > board(i)(j - 1)
              && depth > board(i)(j + 1)) 'X' else depth
          }
        }
      }
    }
  }

  def main(args: Array[String]) {
    val lines = io.Source.stdin.getLines().toList
    printResult(lines.tail)
  }

  def printResult(lines: List[String]): Unit = {
    val board = lines.map(_.toVector).toVector
    val result = cavityMap(board)
    result foreach { line =>
      println(line.mkString)
    }
  }
}