package implementation

import java.text.SimpleDateFormat
import java.util.{Calendar, Date}

/**
 * You are given an array of integers of size N. You need to print the sum of
 * the elements of the array.
 *
 * The Head Librarian at a library wants you to make a program that calculates
 * the fine for returning the book after the return date. You are given the
 * actual and the expected return dates. Calculate the fine as follows:
 *
 * 1. If the book is returned on or before the expected return date, no fine
 *    will be charged, in other words fine is 0.
 * 2. If the book is returned in the same month as the expected return date,
 *    Fine = 15 Hackos x Number of late days
 * 3. If the book is not returned in the same month but in the same year as
 *    the expected return date, Fine = 500 Hackos x Number of late months
 * 4. If the book is not returned in the same year, the fine is fixed at
 *    10000 Hackos.
 *
 * @see https://www.hackerrank.com/challenges/library-fine
 */
object LibraryFine {

  val dateFormat = new SimpleDateFormat("dd MM yyyy")

  def calculateFine(returned: String, due: String): Long = {
    val returnedDate = dateFormat.parse(returned)
    val dueDate = dateFormat.parse(due)
    val returnedTime = returnedDate.getTime()
    val dueTime = dueDate.getTime()
    val timeOverdue = returnedTime - dueTime
    val daysOverdue = timeOverdue / (1000 * 60 * 60 * 24)

    daysOverdue match {
      case _ if (daysOverdue <= 0)                 => 0
      case _ if (sameMonth(returnedDate, dueDate)) => daysOverdue * 15
      case _ if (sameYear(returnedDate, dueDate))  => monthsOverdue(returnedDate, dueDate) * 500
      case _                                       => 10000
    }
  }

  def sameYear(date1: Date, date2: Date): Boolean = {
    val calendar1 = Calendar.getInstance()
    calendar1.setTime(date1)
    val calendar2 = Calendar.getInstance()
    calendar2.setTime(date2)
    val sameYear = calendar1.get(Calendar.YEAR) == calendar2.get(Calendar.YEAR)
    sameYear
  }

  def sameMonth(date1: Date, date2: Date): Boolean = {
    val calendar1 = Calendar.getInstance()
    calendar1.setTime(date1)
    val calendar2 = Calendar.getInstance()
    calendar2.setTime(date2)
    val sameYear = calendar1.get(Calendar.YEAR) == calendar2.get(Calendar.YEAR)
    val sameMonth = calendar1.get(Calendar.MONTH) == calendar2.get(Calendar.MONTH)
    sameMonth && sameYear
  }

  def monthsOverdue(returnedDate: Date, dueDate: Date): Int = {
    val returnedDateCalendar = Calendar.getInstance()
    returnedDateCalendar.setTime(returnedDate)
    val dueDateCalendar = Calendar.getInstance()
    dueDateCalendar.setTime(dueDate)
    returnedDateCalendar.get(Calendar.MONTH) - dueDateCalendar.get(Calendar.MONTH)
  }

  def main(args: Array[String]) {
    val lines = io.Source.stdin.getLines().toArray
    printResult(lines)
  }

  def printResult(lines: Array[String]): Unit = {
    val result = calculateFine(lines(0), lines(1))
    println(result)
  }

}