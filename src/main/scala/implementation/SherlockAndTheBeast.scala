package implementation

/**
 * Sherlock Holmes is getting paranoid about Professor Moriarty, his archenemy.
 * All his efforts to subdue Moriarty have been in vain. These days Sherlock is
 * working on a problem with Dr. Watson. Watson mentioned that the CIA has been
 * facing weird problems with their supercomputer, 'The Beast', recently.
 *
 * This afternoon, Sherlock received a note from Moriarty, saying that he has
 * infected 'The Beast' with a virus. Moreover, the note had the number N printed
 * on it. After doing some calculations, Sherlock figured out that the key to
 * remove the virus is the largest 'Decent' Number having N digits.
 *
 * A 'Decent' Number has -
 *
 * 1. 3 or 5 or both as its digits. No other digit is allowed.
 * 2. Number of times 3 appears is divisible by 5.
 * 3. Number of times 5 appears is divisible by 3.
 *
 * Meanwhile, the counter to destruction of 'The Beast' is running very fast. Can
 * you save 'The Beast', and find the key before Sherlock?
 *
 * @see http://www.hackerrank.com/challenges/sherlock-and-the-beast
 */
object SherlockAndTheBeast {

  /**
   * Find the largest solution to the diophantine equation 3x + 5y = n for given n.
   * None if none exist.
   * @see http://en.wikipedia.org/wiki/Diophantine_equation
   */
  def solveLargestDecent(n: Int): Option[String] = n match {
    case 1                            => None
    case 2                            => None
    case _ if (n % 3 == 0)            => Some("5" * n)
    case _ if (n % 3 == 2 && n >= 5)  => Some("5" * (n - 5) + "3" * 5)
    case _ if (n % 3 == 1 && n >= 10) => Some("5" * (n - 10) + "3" * 10)
    case _ if (n % 5 == 0)            => Some("3" * n)
    case _                            => None
  }

  def main(args: Array[String]) {
    val lines = io.Source.stdin.getLines().toList
    lines.tail.foreach(s => printResult(s))
  }

  def printResult(s: String): Unit = {
    val result = solveLargestDecent(s.toInt)
    result match {
      case None    => println(-1)
      case Some(x) => println(x)
    }
  }

}