package implementation

/**
 * Julius Caesar protected his confidential information from his enemies by
 * encrypting it. Caesar rotated every letter in the string by a fixed number K.
 * This made the string unreadable by the enemy. You are given a string S and
 * the number K. Encrypt the string and print the encrypted string.
 *
 * For example:
 * If the string is middle-Outz and K=2, the encoded string is okffng-Qwvb.
 * Note that only the letters are encrypted while symbols like - are untouched.
 *
 * 'm' becomes 'o' when letters are rotated twice,
 * 'i' becomes 'k',
 * '-' remains the same because only letters are encoded,
 * 'z' becomes 'b' when rotated twice.
 *
 * @see https://www.hackerrank.com/challenges/caesar-cipher-1
 */
object CaesarCipher {

  private val alphaU = 'A' to 'Z'
  private val alphaL = 'a' to 'z'

  def encode(text: String, key: Int): String = text.map {
    case c if alphaU.contains(c) => rotate(alphaU, c, key)
    case c if alphaL.contains(c) => rotate(alphaL, c, key)
    case c => c
  }

  def decode(text: String, key: Int): String = encode(text, -key)

  private def rotate(a: IndexedSeq[Char], c: Char, key: Int) = a((c - a.head + key + a.size) % a.size)

  def main(args: Array[String]) {
    val lines = io.Source.stdin.getLines().toArray
    printResult(lines.tail)
  }

  def printResult(lines: Array[String]): Unit = {
    val text = lines(0)
    val key = lines(1).toInt
    val result = encode(text, key)
    println(result)
  }

}