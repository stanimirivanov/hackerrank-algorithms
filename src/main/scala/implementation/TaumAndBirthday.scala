package implementation

/**
 * Taum is planning to celebrate the birthday of his friend, Diksha. There are
 * two types of gifts that Diksha wants from Taum: one is black and the other is
 * white. To make her happy, Taum has to buy B number of black gifts and W
 * number of white gifts.
 *
 * - The cost of each black gift is X units.
 * - The cost of every white gift is Y units.
 * - The cost of converting each black gift into white gift or vice versa is Z units.
 *
 * Help Taum by deducing the minimum amount he needs to spend on Diksha's gifts.
 *
 * @see https://www.hackerrank.com/challenges/taum-and-bday
 */
object TaumAndBirthday {

  def totalPrice(nrB: Long, nrW: Long, priceB: Long, priceW: Long, priceSwap: Long): Long =
    Math.min(nrB * priceB, nrB * (priceW + priceSwap)) +
      Math.min(nrW * priceW, nrW * (priceB + priceSwap))

  def main(args: Array[String]) {
    val lines = io.Source.stdin.getLines().toList
    lines.tail.grouped(2).foreach(ls => printResult(ls))
  }

  def printResult(lines: List[String]): Unit = {
    val Array(nrB, nrW) = lines.head.split(" ").map(_.toLong)
    val Array(priceB, priceW, priceSwap) = lines.tail.head.split(" ").map(_.toLong)
    val result = totalPrice(nrB, nrW, priceB, priceW, priceSwap)
    println(result)
  }

}