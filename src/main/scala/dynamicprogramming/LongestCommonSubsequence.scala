package dynamicprogramming

/**
 * A subsequence is a sequence that can be derived from another sequence by deleting some elements
 * without changing the order of the remaining elements. Longest common subsequence (LCS) of 2
 * sequences is a subsequence, with maximal length, which is common to both the sequences.
 *
 * Given two sequence of integers, A = [a1, a2, ..., an] and B = [b1, b2, ..., bm], find any one
 * longest common subsequence.
 *
 * In case multiple solutions exist, print any of them. It is guaranteed that at least one non-empty
 * common subsequence will exist.
 *
 * @see http://www.hackerrank.com/challenges/dynamic-programming-classics-the-longest-common-subsequence
 */
object LongestCommonSubsequence {

  def lcs(a: Vector[Int], b: Vector[Int]): Vector[Int] = {
    val lcsTable = buildLcsTable(a, b)
    def backtrack(i: Int, j: Int): Vector[Int] = {
      if (lcsTable(i)(j) == 0) Vector()
      else if (a(i - 1) == b(j - 1)) backtrack(i - 1, j - 1) :+ a(i - 1)
      else if (lcsTable(i - 1)(j) >= lcsTable(i)(j - 1)) backtrack(i - 1, j)
      else backtrack(i, j - 1)
    }

    if (a.isEmpty || b.isEmpty) Vector()
    else if (a == b) a
    else backtrack(a.size, b.size)
  }

  def buildLcsTable(a: Vector[Int], b: Vector[Int]): Array[Array[Int]] = {
    val lsc = Array.ofDim[Int](a.size + 1, b.size + 1)
    for (i <- 0 until a.size) {
      for (j <- 0 until b.size) {
        if (a(i) == b(j)) lsc(i + 1)(j + 1) = lsc(i)(j) + 1
        else lsc(i + 1)(j + 1) = math.max(lsc(i + 1)(j), lsc(i)(j + 1))
      }
    }
    lsc
  }

  def main(args: Array[String]) {
    val lines = io.Source.stdin.getLines().toArray
    val a = lines(1).split(" ").toVector.map(_.toInt)
    val b = lines(2).split(" ").toVector.map(_.toInt)
    printResult(a, b)
  }

  def printResult(a: Vector[Int], b: Vector[Int]): Unit = {
    val result = lcs(a, b)
    println(result.mkString(" "))
  }

}