package sorting

/**
 * Sorting is often useful as the first step in many different tasks. The most
 * common task is to make finding things easier, but there are other uses also.
 *
 * Given a list of unsorted numbers, can you find the numbers that have the
 * smallest absolute difference between them? If there are multiple pairs, find
 * them all.
 *
 * @see https://www.hackerrank.com/challenges/closest-numbers
 */
object ClosestNumbers {

  def closestNumbers(xs: List[Int]): Seq[List[Int]] = {
    val pairs = xs.sorted.sliding(2).toList.sortBy(ls => diff(ls))
    pairs takeWhile { ls => diff(ls) == diff(pairs.head) }
  }

  def diff(xs: List[Int]): Int = xs.tail.head - xs.head

  def main(args: Array[String]) {
    val lines = io.Source.stdin.getLines().toArray
    printResult(lines)
  }

  def printResult(lines: Array[String]): Unit = {
    val xs = lines(1).split(" ").map(_.toInt).toList
    val result = closestNumbers(xs)
    println(result.flatten.mkString(" "))
  }

}