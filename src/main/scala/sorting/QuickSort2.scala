package sorting

/**
 * In the previous challenge, you wrote a partition method to split an array
 * into 2 sub-arrays, one containing smaller elements and one containing larger
 * elements. This means you 'sorted' half the array with respect to the other
 * half. Can you repeatedly use partition to sort an entire array?
 *
 * Guideline In Insertion Sort, you simply went through each element in order
 * and inserted it into a sorted sub-array. In this challenge, you cannot focus
 * on one element at a time, but instead must deal with whole sub-arrays, with a
 * strategy known as "divide and conquer".
 *
 * When partition is called on an array, two parts of the array get 'sorted'
 * with respect to each other. If partition is then called on each sub-array,
 * the array will now be split into 4 parts. This process can be repeated until
 * the sub-arrays are small. Notice that when partition is called on just two
 * numbers, they end up being sorted.
 *
 * Can you repeatedly call partition so that the entire array ends up sorted?
 *
 * Print Sub-Arrays In this challenge, print your array every time your
 * partitioning method finishes, i.e. print every sorted sub-array The first
 * element in a sub-array should be used as a pivot. Partition the left side
 * before partitioning the right side. The pivot should not be added to either
 * side. Instead, put it back in the middle when combining the sub-arrays
 * together.
 *
 * @see https://www.hackerrank.com/challenges/quicksort2
 */
object QuickSort2 {

  def quicksort(xs: List[Int]): (List[Int], List[List[Int]]) = {
    var subs: List[List[Int]] = Nil
    def quicksortInt(xs: List[Int]): List[Int] = {
      if (xs.size < 2) xs
      else {
        val pivot = xs.head
        val (smaller, bigger) = xs.tail partition (_ < pivot)
        val sub = quicksortInt(smaller) ::: pivot :: quicksortInt(bigger)
        subs = sub :: subs
        sub
      }
    }
    (quicksortInt(xs), subs.reverse)
  }

  def main(args: Array[String]) {
    val lines = io.Source.stdin.getLines().toList
    lines.tail.foreach(line => printResult(line))
  }

  def printResult(s: String): Unit = {
    val xs = s.split(" ").map(_.toInt).toList
    val result = quicksort(xs)
    result._2 foreach {
      sub => println(sub.mkString(" "))
    }
  }

}