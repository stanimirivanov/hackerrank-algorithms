package sorting

/**
 * Given an array with n elements, can you sort this array in ascending order
 * using just one of the following operations? You can perform only one of the
 * following operations:
 *
 * 1. Swap two elements.
 * 2. Reverse one sub-segment
 *
 * @see https://www.hackerrank.com/challenges/almost-sorted
 */
object AlmostSorted {

  def isAlmostSorted(xs: Vector[Int]): Seq[String] = {
    val xsorted = xs.zip(xs.sorted)
    val i = xsorted indexWhere { t => t._1 > t._2 }
    val j = xsorted lastIndexWhere { t => t._1 < t._2 }
    if (i == -1) Seq("yes")
    else if (j - i == 1) Seq("yes", s"swap ${i + 1} ${j + 1}")
    else {
      val mids = xs.slice(i + 1, j)
      val midsorted = mids.zip(mids.sorted)
      val i2 = midsorted indexWhere { t => t._1 > t._2 }
      if (mids.size == 1) Seq("no")
      else if (i2 == -1) Seq("yes", s"swap ${i + 1} ${j + 1}")
      else if (mids.reverse == mids.sorted) Seq("yes", s"reverse ${i + 1} ${j + 1}")
      else Seq("no")
    }
  }
  
  def main(args: Array[String]) {
    val lines = io.Source.stdin.getLines().toArray
    printResult(lines)
  }

  def printResult(lines: Array[String]): Unit = {
    val xs = lines(1).split(" ").map(_.toInt).toVector
    val result = isAlmostSorted(xs)
    result foreach { println }
  }

}