package sorting

/**
 * In the Quicksort challenges, you sorted an entire array. Sometimes, you just
 * need specific information about a list of numbers, and doing a full sort
 * would be unnecessary. Can you figure out a way to use your partition code to
 * find the median in an array?
 *
 * @see https://www.hackerrank.com/challenges/find-median
 */
object FindTheMedian {

  def findMedian(xs: Array[Int]): Int = {

    import scala.annotation.tailrec

    @tailrec
    def findKMedian(arr: Array[Int], k: Int): Int = {
      val a = choosePivot(arr)
      val (s, b) = arr partition (_ > a)
      if (s.size == k) a
      else if (s.isEmpty) {
        val (s, b) = arr partition (_ == a)
        if (s.size > k) a
        else findKMedian(b, k - s.size)
      } else if (s.size < k) findKMedian(b, k - s.size)
      else findKMedian(s, k)
    }

    def choosePivot(arr: Array[Int]): Int =
      arr(scala.util.Random.nextInt(arr.size))

    findKMedian(xs, (xs.size - 1) / 2)
  }

  def main(args: Array[String]) {
    val lines = io.Source.stdin.getLines().toList
    lines.tail.foreach(line => printResult(line))
  }

  def printResult(s: String): Unit = {
    val xs = s.split(" ").map(_.toInt).toArray
    val result = findMedian(xs)
    println(result)
  }

}