package sorting

/**
 * In the previous challenge, it was easy to print all the integers in order,
 * since you did not have to access the original list. Once you had obtained the
 * frequencies of all the integers, you could simply print each integer in order
 * the correct number of times. However, if there is other data associated with
 * an element, you will have to access the original element itself.
 *
 * In the final counting sort challenge, you are required to print the data
 * associated with each integer. This means, you will go through the original
 * array to get the data, and then use some "helper arrays" to determine where
 * to place everything in a sorted array.
 *
 * If you know the frequencies of each element, you know how many times to place
 * it, but which index will you start placing it from? It will be helpful to
 * create a helper array for the "starting values" of each element.
 *
 * Challenge: You will be given a list that contains both integers and strings.
 * In this challenge you just care about the integers. For every value i from 0
 * to 99, can you output L, the number of elements that are less than or equal
 * to i?
 *
 * @see https://www.hackerrank.com/challenges/countingsort3
 */
object CountingSort3 {

  def elementsLessOrEqual(xs: Vector[Int], n: Int): Seq[Int] = {
    val counts = xs.groupBy(l => l).map(t => (t._1, t._2.length))

    def loop(acc: Vector[Int], elem: Int): Vector[Int] = {
      if (acc.size == n) acc
      else counts.get(elem) match {
        case Some(count) =>
          val last = if (acc.isEmpty) 0 else acc.last
          loop(acc :+ (count + last), elem + 1)
        case None =>
          loop(acc :+ acc.last, elem + 1)
      }
    }
    loop(Vector(), 0)
  }

  def main(args: Array[String]) {
    val lines = io.Source.stdin.getLines().toVector
    printResult(lines.tail)
  }

  def printResult(lines: Vector[String]): Unit = {
    val xs = lines.map(s => s.split(" ").toList.head.toInt)
    val result = elementsLessOrEqual(xs, 100)
    println(result.mkString(" "))
  }

}