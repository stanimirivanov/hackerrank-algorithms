package sorting

/**
 * Given a word w, rearrange the letters of w to construct another
 * word s in such a way that, s is lexicographically greater than w.
 * In case of multiple possible answers, find the lexicographically
 * smallest one.
 *
 * @see https://www.hackerrank.com/challenges/bigger-is-greater
 */
object BiggerIsGreater {

  def lexicoBigger(s: String): Option[String] = {
    val pivot = s.zip(s.tail).lastIndexWhere {
      case (first, second) => first < second
    }

    if (pivot < 0) {
      None
    } else {
      val successor = s.lastIndexWhere(_ > s(pivot))
      val beginning = s.take(pivot) :+ s(successor)
      val ending = s.slice(pivot + 1, successor) ++ (s(pivot) +: s.drop(successor + 1))
      Some(beginning ++ ending.sorted)
    }
  }

  def main(args: Array[String]) {
    val lines = io.Source.stdin.getLines().toList
    lines.tail.foreach(s => printResult(s))
  }

  def printResult(s: String): Unit = {
    val result = lexicoBigger(s)
    result match {
      case Some(t) => println(t)
      case None    => println("no answer")
    }
  }

}