package sorting

/**
 * The running time of Quicksort will depend on how balanced the partitions are.
 * If you are unlucky and select the greatest or the smallest element as the
 * pivot, then each partition will separate only one element at a time, so the
 * running time will be similar to InsertionSort.
 *
 * However, Quicksort will usually pick a pivot that is mid-range, and it will
 * partition the array into two parts. Let's assume Partition is lucky and it
 * always picks the median element as the pivot. What will be the running time
 * in such a case?
 *
 * Running Time of Recursive Methods Quicksort is a recursive method, so we will
 * have to use a technique to calculate the total running time of all the method
 * calls. We can use a version of the "Recursion Tree Method" to estimate the
 * running time for a given array of N elements.
 *
 * Each time Partition is called on a sub-array, each element in the sub-array
 * needs to be compared with the pivot element. Since all the sub-arrays are
 * passed to partition, there will be N total operations for each level of the
 * tree. How many levels will it take for the Quicksort to finish? Since we
 * assume it always picks the middle element, the array will be split into 2
 * equal halves each time. So it will take log(N) splits until we get single
 * elements in the sub-arrays. Since there are log(N) levels and each one
 * involves N operations, the total running time for Quicksort will be N *
 * log(N).
 *
 * In real sorting, Quicksort won't always pick the exact middle element. But as
 * long as its regularly picking elements near the median value, it will have a
 * running time better than Insertionsort. To make sure that Quicksort works
 * well on most inputs, the real-world implementations do not pick the same
 * index as pivot each time. They use some other technique, e.g., picking a
 * random element. There are other techniques also that can be used to improve
 * Quicksort. The Java Arrays class uses a modified version of Quicksort to sort
 * primitives.
 *
 * Notice that O(N * log(N)) of Quicksort is much much faster than the O(N2) of
 * Insertion Sort. For example, for an array of 1 million elements, N2 = 1012,
 * while N * log(N) is approx. 20 million, a much more manageable number.
 *
 * Challenge In practice, how much faster is Quicksort (in-place) than Insertion
 * Sort? Compare the running time of the two algorithms by counting how many
 * swaps or shifts each one takes to sort an array, and output the difference.
 * You can modify your previous sorting code to keep track of the swaps. The
 * number of swaps required by Quicksort to sort any given input have to be
 * calculated. Keep in mind that the last element of a block is chosen as the
 * pivot, and that the array is sorted in-place as demonstrated in the
 * explanation below.
 *
 * Any time a number is smaller than the partition, it should be "swapped", even
 * if it doesn't actually move to a different location. Also ensure that you
 * count the swap when the pivot is moved into place. The count for Insertion
 * Sort should be the same as the previous challenge, where you just count the
 * number of "shifts".
 *
 * @see https://www.hackerrank.com/challenges/quicksort4
 */
object RunningTimeQuicksort {

  def insertionSort(xs: Array[Int]): Int = {
    var shifts = 0

    def swap(i: Int, j: Int) {
      val temp = xs(i)
      xs(i) = xs(j)
      xs(j) = temp
    }

    (1 until xs.length).foreach { i =>
      (i - 1 to 0 by -1).foreach { j =>
        if (xs(j) > xs(j + 1)) {
          swap(j, j + 1)
          shifts = shifts + 1
        }
      }
    }
    shifts
  }

  def quickSort(xs: Array[Int]): Int = {
    var swaps = 0

    def partition(lo: Int, hi: Int): Int = {
      val pivotIndex = choosePivot(lo, hi)
      val pivotValue = xs(pivotIndex)

      swap(pivotIndex, hi)
      var storeIndex = lo

      for (i <- lo until hi) {
        if (xs(i) < pivotValue) {
          swap(i, storeIndex)
          swaps = swaps + 1
          storeIndex = storeIndex + 1
        }
      }

      swap(storeIndex, hi)
      swaps = swaps + 1
      storeIndex
    }

    def choosePivot(lo: Int, hi: Int): Int = hi

    def swap(i: Int, j: Int) {
      val tmp = xs(i)
      xs(i) = xs(j)
      xs(j) = tmp
    }

    def loop(lo: Int, hi: Int): Unit = {
      if (lo < hi) {
        val part = partition(lo, hi)
        loop(lo, part - 1)
        loop(part + 1, hi)
      }
    }
    loop(0, xs.size - 1)

    swaps
  }

  def operationsDiff(s: String): Int = {
    val xs = s.split(" ").map(_.toInt).toArray
    val ys = s.split(" ").map(_.toInt).toArray
    val isOps = insertionSort(xs)
    val qsOps = quickSort(ys)
    isOps - qsOps
  }

  def main(args: Array[String]) {
    val lines = io.Source.stdin.getLines().toList
    lines.tail.foreach(line => printResult(line))
  }

  def printResult(s: String): Unit = {
    val result = operationsDiff(s)
    println(result)
  }

}