package sorting

/**
 * Often, when a list is sorted, the elements being sorted are just keys to
 * other values. For example, if you are sorting files by their size, the sizes
 * need to stay connected to their respective files. You cannot just take the
 * size numbers and output them in order, you need to output all the required
 * file information.
 *
 * However, if you are not concerned about any other information, then you can
 * simply sort those numbers alone. This makes counting sort very simple. If you
 * already counted the values in the list, you don't need to access the original
 * list again. This challenge involves a simple counting sort where the elements
 * being sorted are all that matter.
 *
 * Challenge: Given an unsorted list of integers, output the integers in order.
 *
 * Hint: You can use your previous code that counted the items to print out the
 * actual values in-order.
 *
 * @see https://www.hackerrank.com/challenges/countingsort2
 */
object CountingSort2 {

  def countingSort(xs: List[Int], n: Int): Seq[Int] = {
    val counts = xs.groupBy(l => l).map(t => (t._1, t._2.length))
    0 until n flatMap { elem =>
      counts.get(elem) match {
        case Some(count) => List.fill(count)(elem)
        case None        => Nil
      }
    }
  }

  def main(args: Array[String]) {
    val lines = io.Source.stdin.getLines().toArray
    printResult(lines)
  }

  def printResult(lines: Array[String]): Unit = {
    val n = lines(0).toInt
    val xs = lines(1).split(" ").map(_.toInt).toList
    val result = countingSort(xs, n)
    println(result.mkString(" "))
  }

}