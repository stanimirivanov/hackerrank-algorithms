package sorting

/**
 * Sorting
 * One common task for computers is to sort data. For example, people might
 * want to see all their files on a computer sorted by size. Since sorting
 * is a simple problem with many different possible solutions, it is often
 * used to introduce the study of algorithms.
 *
 * Insertion Sort
 * These challenges will cover Insertion Sort, a simple and intuitive sorting algorithm.
 * We will first start with an already sorted list.
 *
 * Insert element into sorted list
 * Given a sorted list with an unsorted number V in the right-most cell, can you write
 * some simple code to insert V into the array so it remains sorted?
 *
 * Print the array every time a value is shifted in the array until the array is fully
 * sorted. The goal of this challenge is to follow the correct order of insertion sort.
 *
 * Guideline: You can copy the value of V to a variable, and consider its cell "empty".
 * Since this leaves an extra cell empty on the right, you can shift everything over
 * until V can be inserted. This will create a duplicate of each value, but when you
 * reach the right spot, you can replace a value with V.
 *
 * @see http://www.hackerrank.com/challenges/insertionsort1
 */
object InsertionSort1 {

  import scala.util.control.Breaks._
  import scala.collection.mutable.Buffer

  def insertionSort(xs: Buffer[Int]): Unit = {
    var inserted = false
    val last = xs.last
    breakable {
      for (i <- xs.size - 2 to 0 by -1) {
        if (xs(i) > last) {
          xs(i + 1) = xs(i)
          println(xs.mkString(" "))
        } else {
          xs(i + 1) = last
          inserted = true
          println(xs.mkString(" "))
          break
        }
      }
      if (!inserted) {
        xs(0) = last
        println(xs.mkString(" "))
      }
    }
  }

  def printBuffer(xs: Buffer[Int]): Unit = {
    println(xs.mkString(" "))
  }

  def main(args: Array[String]) {
    val lines = io.Source.stdin.getLines().toList
    lines.tail.foreach(line => printResult(line))
  }

  def printResult(s: String): Unit = {
    val xs = s.split(" ").map(_.toInt).toBuffer
    insertionSort(xs)
  }

}