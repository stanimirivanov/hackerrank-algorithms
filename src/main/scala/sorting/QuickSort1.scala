package sorting

/**
 * You're given an array ar and a number p. Partition the array, so that, all
 * elements greater than p are to its right, and all elements smaller than p are
 * to its left.
 *
 * In the new sub-array, the relative positioning of elements should remain the
 * same, i.e., if n1 was before n2 in the original array, it must remain before
 * it in the sub-array. The only situation where this does not hold good is when
 * p lies between n1 and n2
 *
 * i.e., n1 > p > n2.
 *
 * Guideline - In this challenge, you do not need to move around the numbers
 * 'in-place'. This means you can create 2 lists and combine them at the end.
 *
 * @see https://www.hackerrank.com/challenges/quicksort1
 */
object QuickSort1 {

  def partition(xs: List[Int]): List[Int] = {
    val pivot = xs.head
    val part = xs partition (_ < pivot)
    part._1 ::: part._2
  }

  def main(args: Array[String]) {
    val lines = io.Source.stdin.getLines().toList
    lines.tail.foreach(line => printResult(line))
  }

  def printResult(s: String): Unit = {
    val xs = s.split(" ").map(_.toInt).toList
    val result = partition(xs)
    println(result.mkString(" "))
  }

}