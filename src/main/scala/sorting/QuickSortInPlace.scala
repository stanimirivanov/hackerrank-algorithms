package sorting

/**
 * Create an in-place version of Quicksort. Also, always select the last element
 * in the 'sub-array' as a pivot. Partition the left side and then the right
 * side of the array. Print out the whole array at the end of every partitioning
 * method.
 *
 * Guideline: Instead of copying the array into multiple sub-arrays, use indices
 * to keep track of the different sub-arrays. You can pass the indices to a
 * modified partition method. The partition method should partition the
 * sub-array and then return the index location where the pivot gets placed, so
 * you can then call partition on each side of the pivot.
 *
 * Since you cannot just create new sub-arrays for the elements, Partition will
 * need to use another trick to keep track of which elements are greater and
 * which are smaller than the pivot.
 *
 * The In-place Trick: If an element is smaller than the Pivot, you should swap
 * it with a (larger) element on the left-side of the sub-array. Large elements
 * can just remain where they are, and the pivot can then be inserted in the
 * middle at the end of the partition method. To ensure that you don't swap a
 * small element with another small element, use an index to keep track of the
 * small elements that have already been swapped into their "place". Make sure
 * to always swap with an element to the right of the "small" elements.
 *
 * @see https://www.hackerrank.com/challenges/quicksort3
 */
object QuickSortInPlace {

  def quicksort(xs: Array[Int]): List[List[Int]] = {
    var subs: List[List[Int]] = Nil

    def partition(lo: Int, hi: Int): Int = {
      val pivotIndex = choosePivot(lo, hi)
      val pivotValue = xs(pivotIndex)

      swap(pivotIndex, hi)
      var storeIndex = lo
      
      for (i <- lo until hi) {
        if (xs(i) < pivotValue) {
          swap(i, storeIndex)
          storeIndex = storeIndex + 1
        }
      }
      
      swap(storeIndex, hi)
      storeIndex
    }

    def choosePivot(lo: Int, hi: Int): Int = hi

    def swap(i: Int, j: Int) {
      val tmp = xs(i)
      xs(i) = xs(j)
      xs(j) = tmp
    }

    def loop(lo: Int, hi: Int): Unit = {
      if (lo < hi) {
        val part = partition(lo, hi)
        subs =  (0 until xs.size).map(xs(_)).toList :: subs
        loop(lo, part - 1)
        loop(part + 1, hi)
      }
    }
    loop(0, xs.size - 1)
    
    subs.reverse
  }

  def main(args: Array[String]) {
    val lines = io.Source.stdin.getLines().toList
    lines.tail.foreach(line => printResult(line))
  }

  def printResult(s: String): Unit = {
    val xs = s.split(" ").map(_.toInt).toArray
    val result = quicksort(xs)
    result foreach {
      sub => println(sub.mkString(" "))
    }
  }

}