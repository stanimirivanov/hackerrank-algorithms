package sorting

/**
 * In Insertion Sort Part 1, you sorted one element into an array.
 * Using the same approach repeatedly, can you sort an entire unsorted array?
 *
 * Guideline: You already can place an element into a sorted array. How can you
 * use that code to build up a sorted array, one element at a time? Note that
 * in the first step, when you consider an element with just the first element -
 * that is already "sorted" since there's nothing to its left that is smaller than it.
 *
 * In this challenge, don't print every time you move an element. Instead, print
 * the array every time an element is "inserted" into the array in (what is currently)
 * its correct place. Since the array composed of just the first element is already
 * "sorted", begin printing from the second element and on.
 *
 * @see http://www.hackerrank.com/challenges/insertionsort2
 */
object InsertionSort2 {

  import scala.collection.mutable.Buffer

  def insertionSort(xs: Buffer[Int]): Unit = {
    (1 until xs.length).foreach { i =>
      (i - 1 to 0 by -1).foreach { j => 
        if (xs(j) > xs(j + 1)) swap(xs, j, j + 1)
      }
      println(xs.mkString(" "))
    }
  }

  def swap(xs: Buffer[Int], i: Int, j: Int) {
    val temp = xs(i)
    xs(i) = xs(j)
    xs(j) = temp
  }

  def printBuffer(xs: Buffer[Int]): Unit = {
    println(xs.mkString(" "))
  }

  def main(args: Array[String]) {
    val lines = io.Source.stdin.getLines().toList
    lines.tail.foreach(line => printResult(line))
  }

  def printResult(s: String): Unit = {
    val xs = s.split(" ").map(_.toInt).toBuffer
    insertionSort(xs)
  }

}