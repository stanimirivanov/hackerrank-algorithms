package sorting

/**
 * Comparison Sorting: Quicksort usually has a running time of n*log(n), but is
 * there an algorithm that can sort even faster? In general, this is not
 * possible. Most sorting algorithms are comparison sorts, i.e., they sort a
 * list just by comparing the elements with one another. A comparison sort
 * algorithm cannot beat n log(n) (worst-case) running time, since n log(n)
 * represents the minimum number of comparisons needed to know where to place
 * each element. For more details, you can see these notes
 * [http://www.cs.cmu.edu/~avrim/451f11/lectures/lect0913.pdf] (PDF).
 *
 * Alternative Sorting: However, for certain types of input, it is more
 * efficient to use a non-comparison sorting algorithm. This will make it
 * possible to sort lists even in linear time. These challenges will cover
 * Counting Sort, a fast way to sort lists where the elements have a small
 * number of possible values, such as integers within a certain range. We will
 * start with an easy task - counting.
 *
 * Challenge: Given a list of integers, can you count and output the number of
 * times each value appears?
 *
 * Hint: There is no need to sort the data, you just need to count it.
 *
 * @see https://www.hackerrank.com/challenges/countingsort1
 */
object CountingSort1 {

  def countingSort(xs: List[Int], n: Int): Seq[Int] = {
    val counts = xs.groupBy(l => l).map(t => (t._1, t._2.length))
    0 until n map { i => counts.getOrElse(i, 0) }
  }

  def main(args: Array[String]) {
    val lines = io.Source.stdin.getLines().toArray
    printResult(lines)
  }

  def printResult(lines: Array[String]): Unit = {
    val n = lines(0).toInt
    val xs = lines(1).split(" ").map(_.toInt).toList
    val result = countingSort(xs, n)
    println(result.take(math.min(100, result.size)).mkString(" "))
  }

}