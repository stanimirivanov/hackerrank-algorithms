package sorting

/**
 * Insertion Sort and the simple version of QuickSort were stable, but the
 * faster in-place version of Quicksort was not (since it scrambled around
 * elements while sorting).
 *
 * In cases where you care about the original order, it is important to use a
 * stable sorting algorithm. In this challenge, you will use counting sort to
 * sort a list while keeping the order of the strings (with same accompanying
 * integer) preserved.
 *
 * Challenge: In the previous challenge, you created a "helper array" that
 * contains information about the starting position of each element in a sorted
 * array. Can you use this array to help you create a sorted array of the
 * original list?
 *
 * Hint: You can go through the original array to access the strings. You can
 * then use your helper array to help determine where to place those strings in
 * the sorted array. Be careful about being one off.
 *
 * Details and a Twist: You will be given a list that contains both integers and
 * strings. Can you print the strings in order of their accompanying integers?
 * If the integers for two strings are equal, ensure that they are print in the
 * order they appeared in the original list.
 *
 * The Twist - Your clients just called with an update. They don't want you to
 * print the first half of the original array. Instead, they want you to print a
 * dash for any element from the first half. So you can modify your counting
 * sort algorithm to sort the second half of the array only.
 *
 * @see https://www.hackerrank.com/challenges/countingsort4
 */
object FullCountingSort {

  import scala.collection.mutable.StringBuilder

  case class Item(num: Int, value: String)

  def countingSort(items: Vector[Item], size: Int): String = {
    val half = items.size / 2
    val sbs = Array.fill(size)(new StringBuilder())
    0 until items.size foreach { i =>
      val item = items(i)
      val sb = sbs(item.num)
      if (i < half) sb.append("-") else sb.append(item.value)
      sb.append(" ")
    }
    sbs.tail.foldLeft(sbs.head)((acc, sb) => acc.append(sb)).toString.trim
  }

  def main(args: Array[String]) {
    val lines = io.Source.stdin.getLines().toVector
    printResult(lines)
  }

  def printResult(lines: Vector[String]): Unit = {
    val items = lines.tail.map { s =>
      val nv = s.split(" ")
      Item(nv(0).toInt, nv(1))
    }
    val result = countingSort(items, 100)
    println(result)
  }

}