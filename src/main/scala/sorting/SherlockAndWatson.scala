package sorting

/**
 * John Watson performs an operation called Right Circular Rotation on an
 * integer array a0, a1 ... an-1. Right Circular Rotation transforms the array
 * from a0, a1 ... aN-1 to aN-1, a0,... aN-2.
 *
 * He performs the operation K times and tests Sherlock's ability to identify
 * the element at a particular position in the array. He asks Q queries. Each
 * query consists of one integer x, for which you have to print the element ax.
 *
 * @see https://www.hackerrank.com/challenges/sherlock-and-watson
 */
object SherlockAndWatson {

  class RingBuffer[A](val index: Int, val data: IndexedSeq[A]) extends IndexedSeq[A] {
    def shiftLeft = new RingBuffer((index + 1) % data.size, data)
    def shiftRight = new RingBuffer((index + data.size - 1) % data.size, data)
    def length = data.length
    def apply(i: Int) = data((index + i) % data.size)
  }

  def queryAfterRotatins(xs: Vector[Int], queries: List[Int], rotations: Int): Seq[Int] = {
    val index = xs.size - rotations % xs.size
    val rb = new RingBuffer(index, xs)
    queries map { q => rb(q) }
  }

  def main(args: Array[String]) {
    val lines = io.Source.stdin.getLines().toArray
    printResult(lines)
  }

  def printResult(lines: Array[String]): Unit = {
    val k = lines(0).split(" ").map(_.toInt).tail.head
    val xs = lines(1).split(" ").map(_.toInt).toVector
    val qs = lines.drop(2).map(_.toInt).toList
    val result = queryAfterRotatins(xs, qs, k)
    result foreach { r =>
      println(r)
    }
  }

}