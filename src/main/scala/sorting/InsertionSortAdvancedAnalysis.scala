package sorting

/**
 * Insertion Sort is a simple sorting technique which was covered in previous
 * challenges. Sometimes, arrays may be too large for us to wait around for
 * insertion sort to finish. Is there some other way we can calculate the number
 * of times Insertion Sort shifts each elements when sorting an array?
 *
 * If k_i is the number of elements over which i-th element of the array has to
 * shift then total number of shift will be k_1 + k_2 + ... + k_N.
 *
 * @see https://www.hackerrank.com/challenges/insertion-sort
 * @see http://www.quora.com/How-can-I-efficiently-compute-the-number-of-swaps-required-by-slow-sorting-methods-like-insertion-sort-and-bubble-sort-to-sort-a-given-array
 */
object InsertionSortAdvancedAnalysis {

  class FenwickTree(val size: Int) {

    val tree: Array[Int] = Array.fill(size + 1)(0)

    def add(idx: Int, value: Int): Unit = {
      def loop(idx: Int): Unit = {
        if (idx > size) ()
        else {
          tree(idx) = tree(idx) + value
          loop(idx + (idx & -idx))
        }
      }
      loop(idx + 1)
    }

    def sum(idx: Int): Long = {
      def loop(idx: Int, acc: Long): Long = {
        if (idx <= 0) acc
        else {
          val res = acc + tree(idx)
          loop(idx - (idx & -idx), res)
        }
      }
      loop(idx + 1, 0)
    }

  }

  def count(xs: Array[Int]): Long = {
    val bit = new FenwickTree(xs.max + 1)
    xs.zipWithIndex.foldLeft(0L) { (acc, t) =>
      val res = t._2 - bit.sum(t._1)
      bit.add(t._1, 1)
      acc + res
    }
  }

  def main(args: Array[String]) {
    val lines = io.Source.stdin.getLines().toList
    lines.tail.grouped(2).foreach(ls => printResult(ls))
  }

  def printResult(lines: List[String]): Unit = {
    val xs = lines.tail.head.split(" ").map(_.toInt).toArray
    val result = count(xs)
    println(result)
  }

}