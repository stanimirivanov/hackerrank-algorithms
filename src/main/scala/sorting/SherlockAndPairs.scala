package sorting

/**
 * John Watson performs an operation called Right Circular Rotation on an
 * integer array a0, a1 ... an-1. Right Circular Rotation transforms the array
 * from a0, a1 ... aN-1 to aN-1, a0,... aN-2.
 *
 * Sherlock is given an array of N integers (A_0, A_1 ... A_N?1 by Watson. Now
 * Watson asks Sherlock how many different pairs of indices i and j exist such
 * that i is not equal to j but A_i is equal to A_j.
 *
 * That is, Sherlock has to count the total number of pairs of indices (i, j)
 * where A_i = A_j AND i =/= j.
 *
 * @see https://www.hackerrank.com/challenges/sherlock-and-pairs
 */
object SherlockAndPairs {

  def nCk(n: Int, k: Int): Long = {
    if (n < k) 0
    else {
      val sum = (0 until k).foldLeft(0.toDouble) {
        (acc, x) => acc + Math.log10(n - x) - Math.log10(x + 1)
      }
      Math.round(Math.pow(10, sum))
    }
  }

  def countPairs(xs: Vector[Int]): BigInt = {
    val segments = xs.groupBy(identity).values.toSeq
    segments.map(_.size).foldLeft(BigInt(0)) {
      (acc, n) => acc + nCk(n, 2) * 2
    }
  }

  def main(args: Array[String]) {
    val lines = io.Source.stdin.getLines().toList
    lines.tail grouped (2) foreach {
      printResult(_)
    }
  }

  def printResult(lines: List[String]): Unit = {
    val xs = lines(1).split(" ").map(_.toInt).toVector
    val result = countPairs(xs)
    println(result)
  }

}