package warmup

/**
 * You're given an array containing integer values. You need to print the
 * fraction of count of positive numbers, negative numbers and zeroes to the
 * total numbers. Print the value of the fractions correct to 3 decimal places.
 *
 * @see https://www.hackerrank.com/challenges/plus-minus
 */
object PlusMinus {

  implicit class ExtendedDouble(d: Double) {
    def rounded(n: Int) = {
      BigDecimal(d).setScale(n, BigDecimal.RoundingMode.HALF_UP).toDouble
    }
  }

  def fractionCounts(nums: List[Int]): (Double, Double, Double) = {
    val negatives = nums filter (_ < 0)
    val zeros = nums filter (_ == 0)
    val positives = nums filter (_ > 0)
    val count = nums.size.toDouble
    ((positives.size / count).rounded(6),
      (negatives.size / count).rounded(6),
      (zeros.size / count).rounded(6))
  }

  def main(args: Array[String]) {
    val lines = io.Source.stdin.getLines().toList
    printResult(lines.tail.head)
  }

  def printResult(s: String): Unit = {
    val nums = s.split(" ").map(_.toInt).toList
    val result = fractionCounts(nums)
    println(result._1)
    println(result._2)
    println(result._3)
  }

}