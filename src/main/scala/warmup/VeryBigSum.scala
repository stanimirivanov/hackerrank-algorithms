package warmup

/**
 * You are given an array of integers of size N. You need to print the sum of
 * the elements of the array.
 *
 * Note: A signed 32-bit integer value uses 1st bit to represent the sign of the
 * number and remaining 31 bits to represent the magnitude. The range of the
 * 32-bit integer is ?231 to 231?1 or [?2147483648,2147483647]. When we add
 * several integer values, the resulting sum might exceed this range. You might
 * need to use long long int in C/C++ or long data type in Java to store such
 * sums.
 *
 * @see https://www.hackerrank.com/challenges/a-very-big-sum
 */
object VeryBigSum {

  def veryBigSum(xs: List[Long]): Long =
    xs.sum

  def main(args: Array[String]) {
    val lines = io.Source.stdin.getLines().toList
    printResult(lines.tail.head)
  }

  def printResult(s: String): Unit = {
    val xs = s.split(" ").map(_.toLong).toList
    val result = veryBigSum(xs)
    println(result)
  }

}