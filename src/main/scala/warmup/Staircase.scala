package warmup

import scala.collection.immutable.IndexedSeq

/**
 * Your teacher has given you the task to draw the structure of a staircase.
 * Being an expert programmer, you decided to make a program for the same. You
 * are given the height of the staircase. You need to print a staircase as
 * shown in the example.
 *
 * @see https://www.hackerrank.com/challenges/staircase
 */
object Staircase {

  def staircase(n: Int): IndexedSeq[String] =
    for {
      i <- 1 to n
    } yield (" " * (n - i)) + ("#" * i)

  def main(args: Array[String]) {
    val lines = io.Source.stdin.getLines().toList
    printResult(lines.head)
  }

  def printResult(s: String): Unit = {
    val result = staircase(s.toInt)
    result foreach {
      line => println(line)
    }
  }

}