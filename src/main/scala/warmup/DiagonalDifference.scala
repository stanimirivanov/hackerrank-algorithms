package warmup

import scala.collection.immutable.IndexedSeq

/**
 * You are given a square matrix of size NxN. Calculate the absolute difference
 * of the sums across the two main diagonals.
 *
 * @see https://www.hackerrank.com/challenges/diagonal-difference
 */
object DiagonalDifference {

  class Matrix(elements: Vector[Vector[Int]]) {
    def nRows: Int = elements.length

    def nCols: Int = if (elements.isEmpty) 0 else elements.head.length

    def diagonal: IndexedSeq[Int] = {
      require(nRows > 0 && nCols > 0 && nRows == nCols)
      for {
        i <- 0 until nRows
        j <- 0 until nCols
        if (i == j)
      } yield elements(i)(j)
    }

    def antidiagonal: IndexedSeq[Int] = {
      require(nRows > 0 && nCols > 0 && nRows == nCols)
      for {
        i <- 0 until nRows
        j <- 0 until nCols
        if (i + j == nRows - 1)
      } yield elements(i)(j)
    }

    def trace: Int = diagonal.sum

    def antitrace: Int = antidiagonal.sum
  }

  object Matrix {
    def apply(lines: List[String]): Matrix = {
      val rows: Vector[Vector[Int]] = (lines map { line: String =>
        (line.split(" ") map { s: String => s.toInt }).toVector
      }).toVector
      new Matrix(rows)
    }
  }

  def diagonalDifference(m: Matrix): Int = Math.abs(m.trace - m.antitrace)

  def main(args: Array[String]) {
    val lines = io.Source.stdin.getLines().toList
    printResult(lines.tail)
  }

  def printResult(lines: List[String]): Unit = {
    val m = Matrix(lines)
    val result = diagonalDifference(m)
    println(result)
  }

}