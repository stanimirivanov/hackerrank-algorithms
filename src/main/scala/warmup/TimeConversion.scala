package warmup

import java.text.SimpleDateFormat

/**
 * You are given time in AM/PM format. Convert this into a 24 hour format.
 *
 * Note Midnight is 12:00:00AM or 00:00:00 and 12 Noon is 12:00:00.
 *
 * @see https://www.hackerrank.com/challenges/time-conversion
 */
object TimeConversion {

  val inputFormat = new SimpleDateFormat("hh:mm:ssa")
  val outputFormat = new SimpleDateFormat("HH:mm:ss")

  def convert(s: String): String = {
    val date = inputFormat.parse(s)
    outputFormat.format(date)
  }

  def main(args: Array[String]) {
    val lines = io.Source.stdin.getLines().toList
    printResult(lines.head)
  }

  def printResult(s: String): Unit = {
    val result = convert(s)
    println(result)
  }

}