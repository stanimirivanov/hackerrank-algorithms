package bitmanipulation

/**
 * There are N integers in an array A. All but one integer
 * occur in pairs. Your task is to find out the number that
 * occurs only once.
 *
 * @see https://www.hackerrank.com/challenges/lonely-integer
 */
object LonelyInteger {

  /**
   * The bitwise XOR (^) of two numbers equals 0,
   * so the bitwise XOR sum of all numbers in the
   * list will yield the lonely number.
   */
  def findLonelyInt(xs: List[Int]): Int =
    xs.reduce(_ ^ _)

  def main(args: Array[String]) {
    val lines = io.Source.stdin.getLines().toList
    printResult(lines.tail.head)
  }

  def printResult(s: String): Unit = {
    val xs = s.split(" ").toList.map(_.toInt)
    val result = findLonelyInt(xs)
    println(result)
  }
}