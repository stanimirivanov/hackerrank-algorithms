package bitmanipulation

/**
 * You will be given a list of 32-bits unsigned integers.
 * You are required to output the list of the unsigned integers you get
 * by flipping bits in its binary representation (i.e. unset bits must
 * be set, and set bits must be unset).
 *
 * @see http://www.hackerrank.com/challenges/flipping-bits
 */
object FlippingBits {

  /**
   * 0x00000000ffffffffL will up-convert a java signed int
   * to 64bit long
   * @see http://en.wikipedia.org/wiki/Bit_flipping
   */
  def flipBits(n: Long): Long =
    ~(n.toLong) & 0x00000000ffffffffL

  def main(args: Array[String]) {
    val lines = io.Source.stdin.getLines().drop(1).toList
    lines.foreach(line => printResult(line))
  }

  def printResult(s: String): Unit = {
    val result = flipBits(s.toLong)
    println(result)
  }

}