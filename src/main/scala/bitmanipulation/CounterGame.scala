package bitmanipulation

/**
 * Louise and Richard play a game. They have a counter set to N. Louise gets the
 * first turn and the turns alternate thereafter. In the game, they perform the
 * following operations.
 *
 * - If N is not a power of 2, reduce the counter by the largest power of 2 less
 * than N.
 * - If N is a power of 2, reduce the counter by half of N.
 * - The resultant value is the new N which is again used for subsequent operations.
 *
 * The game ends when the counter reduces to 1, i.e., N == 1, and the last person
 * to make a valid move wins.
 *
 * Given N, your task is to find the winner of the game.
 *
 * Update If they set counter to 1, Richard wins, because its Louise' turn and
 * she cannot make a move.
 *
 * @see https://www.hackerrank.com/challenges/counter-game
 */
object CounterGame {

  def playGame(n: BigInt): Int = {
    def loop(n: BigInt, counter: Int): Int = {
      if (n == 1) counter
      else if ((n & (n - 1)) == 0) loop(n >> 1, (counter + 1) % 2)
      else loop(n - largestPowerLessThan(n), (counter + 1) % 2)
    }

    def largestPowerLessThan(n: BigInt): BigInt = {
      if ((n & (n - 1)) == 0) n
      else largestPowerLessThan(n & (n - 1))
    }

    loop(n, 0)
  }

  def main(args: Array[String]) {
    val lines = io.Source.stdin.getLines().toList
    lines.tail.foreach(line => printResult(line))
  }

  def printResult(line: String): Unit = {
    val n = BigInt(line)
    val result = playGame(n)
    if (result == 1) println("Louise") else println("Richard")
  }

}