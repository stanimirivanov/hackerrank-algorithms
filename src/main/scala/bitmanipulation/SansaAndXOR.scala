package bitmanipulation

/**
 * Sansa has an array. She wants to find the value obtained by XOR-ing the contiguous
 * subarrays, followed by XOR-ing the values thus obtained. Can you help her in this task?
 *
 * Note : [1,2,3] is contiguous subarray of [1,2,3,4] while [1,2,4] is not.
 *
 * @see https://www.hackerrank.com/challenges/sansa-and-xor
 */
object SansaAndXOR {

  def xor(xs: List[Int]): Int = {
    val n = xs.size
    if (n % 2 == 0) 0
    else xs.zipWithIndex.foldLeft(0)((acc, t) => if (t._2 % 2 == 0) acc ^ t._1 else acc)
  }

  def main(args: Array[String]) {
    val lines = io.Source.stdin.getLines().toList
    lines.tail.grouped(2).foreach(ls => printResult(ls))
  }

  def printResult(ls: List[String]): Unit = {
    val xs = ls.tail.head.split(" ").map(_.toInt).toList
    val result = xor(xs)
    println(result)
  }
}