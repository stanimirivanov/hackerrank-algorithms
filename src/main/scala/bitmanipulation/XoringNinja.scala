package bitmanipulation

/**
 * Given a list containing N integers, calculate the XorSum of all the non-empty
 * subsets of the list and print the value of XorSum % (109+7).
 *
 * XOR operation on a list is defined as the xor, of all the elements present in
 * it, e.g., XOR({A, B, C}) = A xor B xor C.
 *
 * XorSum of a list is the sum of the XORs of every possible non-empty subset of
 * the list. For example, for list X = {x1, x2, x3} all possible non-empty
 * subsets are [{x1}, {x2}, {x3}, {x1, x2}, {x1, x3}, {x2, x3}, {x1, x2, x3}]
 * XorSum(X) = x1 + x2 + x3 +(x1 xor x2) + (x1 xor x3) + (x2 xor x3) +
 *          (x1 xor x2 xor x3)
 *
 * @see https://www.hackerrank.com/challenges/xoring-ninja
 */
object XoringNinja {

  def xorSum(xs: List[BigInt]): BigInt =
    xs.foldLeft(BigInt(0))((acc, x) => acc | x) * BigInt(2).pow(xs.size - 1)

  def main(args: Array[String]) {
    val lines = io.Source.stdin.getLines().toList
    lines.tail.grouped(2).foreach(ls => printResult(ls))
  }

  def printResult(ls: List[String]): Unit = {
    val xs = ls.tail.head.split(" ").map(BigInt(_)).toList
    val result = xorSum(xs)
    println(result % 1000000007)
  }

}