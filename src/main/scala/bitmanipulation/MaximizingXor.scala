package bitmanipulation

/**
 * Given two integers: L and R, find the maximal values of A xor B given, L ≤ A ≤ B ≤ R
 *
 * @see http://www.hackerrank.com/challenges/maximizing-xor
 */
object MaximizingXor {

  def maxXor(l: Int, r: Int): Int = {
    val range = (l to r)
    val candidates = for {
      a <- range
      b <- range
    } yield a ^ b
    candidates.max
  }

  def main(args: Array[String]) {
    val lines = io.Source.stdin.getLines().toArray
    printResult(lines)
  }

  def printResult(lines: Array[String]): Unit = {
    val l = lines(0).toInt
    val r = lines(1).toInt
    val result = maxXor(l, r)
    println(result)
  }

}