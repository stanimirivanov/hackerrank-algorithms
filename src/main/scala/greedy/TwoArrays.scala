package greedy

/**
 * You are given two integer arrays, A and B, each containing N integers.
 * The size of the array is less than or equal to 1000. You are free to
 * permute the order of the elements in the arrays.
 *
 * Now here's the real question: Is there an permutation A', B' possible
 * of A and B, such that, A'i + B'i >= K for all i, where A'i denotes the i-th
 * element in the array A' and B'i denotes i-th element in the array B'.
 *
 * @see https://www.hackerrank.com/challenges/two-arrays
 */
object TwoArrays {

  def findPermutation(as: List[Int], bs: List[Int], k: Int): Option[(List[Int], List[Int])] = {
    def loop(as: List[Int], bs: List[Int], acc: (List[Int], List[Int])): Option[(List[Int], List[Int])] = {
      if (as.isEmpty) Some(acc)
      else if (as.head + bs.head >= k) loop(as.tail, bs.tail, (as.head :: acc._1, bs.head :: acc._2))
      else None
    }

    loop(as.sorted, bs.sortWith(_ > _), (Nil, Nil))
  }

  def main(args: Array[String]) {
    val lines = io.Source.stdin.getLines().toList
    lines.tail.grouped(3).foreach(ls => printResult(ls))
  }

  def printResult(ls: List[String]): Unit = {
    val k = ls.head.split(" ").map(_.toInt).toList.tail.head
    val as = ls.tail.head.split(" ").map(_.toInt).toList
    val bs = ls.tail.tail.head.split(" ").map(_.toInt).toList
    val result = findPermutation(as, bs, k)
    result match {
      case Some(_) => println("YES")
      case None    => println("NO")
    }
  }
}