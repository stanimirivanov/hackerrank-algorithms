package greedy

/**
 * Alice gives a wooden board composed of M X N wooden square pieces to Bob and
 * asks him to find the minimal cost of breaking the board into square wooden
 * pieces. Bob can cut the board along horizontal and vertical lines, and each
 * cut divides the board in smaller parts. Each cut has a cost depending on
 * whether the cut is made along a horizontal or a vertical line.
 *
 * Let us denote the costs of cutting it along consecutive vertical lines with
 * x1, x2, ..., xn-1, and the cost of cutting it along horizontal lines with y1,
 * y2, ..., ym-1. If a cut (of cost c) is made and it passes through n segments,
 * then total cost of this cut will be n*c.
 *
 * The cost of cutting the whole board into single squares is the sum of the
 * cost of successive cuts used to cut the whole board into square wooden pieces
 * of size 1x1. Bob should compute the minimal cost of breaking the whole wooden
 * board into squares of size 1x1.
 *
 * Bob needs your help to find the minimal cost. Can you help Bob with this
 * challenge?
 *
 * @see https://www.hackerrank.com/challenges/board-cutting
 */
object CuttingBoards {

  trait Direction {
    def isVertical(): Boolean
    def value(): BigInt
  }

  case class Vertical(value: BigInt) extends Direction {
    def isVertical(): Boolean = true
  }

  case class Horizontal(value: BigInt) extends Direction {
    def isVertical(): Boolean = false
  }

  def minimizeCutting(xs: List[BigInt], ys: List[BigInt]): BigInt = {
    def loop(ds: List[Direction], v: Int, h: Int, cost: BigInt): BigInt = {
      if (ds.isEmpty) cost
      else {
        if (ds.head.isVertical) loop(ds.tail, v, h + 1, cost + ((v + 1) * ds.head.value))
        else loop(ds.tail, v + 1, h, cost + ((h + 1) * ds.head.value))
      }
    }
    
    val ds = (xs.map(Vertical(_)) ::: ys.map(Horizontal(_))).sortWith(_.value > _.value)
    loop(ds, 0, 0, BigInt(0))
  }

  def main(args: Array[String]) {
    val lines = io.Source.stdin.getLines().toList
    lines.tail.grouped(3).foreach(ls => printResult(ls))
  }

  def printResult(ls: List[String]): Unit = {
    val xs = ls.tail.head.split(" ").map(BigInt(_)).toList
    val ys = ls.tail.tail.head.split(" ").map(BigInt(_)).toList
    val result = minimizeCutting(0 :: xs, 0 :: ys)
    println(result % 1000000007)
  }
}