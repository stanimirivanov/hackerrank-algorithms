package greedy

/**
 * Bill Gates is on one of his philanthropic journeys to a village in Utopia.
 * He has N packets of candies and would like to distribute one packet to each
 * of the K children in the village (each packet may contain different number of candies).
 * To avoid any fighting among the children, he would like to pick K out of N packets,
 * such that unfairness is minimized.
 *
 * Suppose the K packets have (x1, x2, x3,....xk) candies in them, where x_i denotes the
 * number of candies in the i-th packet, then we define unfairness as
 *
 * max(x1, x2, ... xk) - min(x1, x2, ... xk)
 *
 * where max denotes the highest value amongst the elements, and min denotes the least
 * value amongst the elements. Can you figure out the minimum unfairness and print it?
 *
 * @see http://www.hackerrank.com/challenges/angry-children
 */
object AngryChildren {

  def minimizeUnfairness(nrKids: Int, packets: Vector[Int]): Int = {
    val sortedPackets = packets.sorted
    (0 until packets.size - nrKids).foldLeft(Int.MaxValue) { (unfairness, i) =>
      val tmp = sortedPackets(nrKids + i - 1) - sortedPackets(i)
      if (tmp < unfairness) tmp else unfairness
    }
  }

  def main(args: Array[String]) {
    val lines = io.Source.stdin.getLines().toList
    printResult(lines)
  }

  def printResult(lines: List[String]): Unit = {
    val nrKids = lines.tail.head.toInt
    val packets = lines.drop(2).map(_.toInt).toVector
    println(minimizeUnfairness(nrKids, packets))
  }

}