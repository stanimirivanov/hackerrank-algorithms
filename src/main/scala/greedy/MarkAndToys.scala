package greedy

/**
 * Mark and Jane are very happy after having their first kid. Their
 * son is very fond of toys, so Mark wants to buy some. There are N
 * different toys lying in front of him, tagged with their prices,
 * but he has only $K. He wants to maximize the number of toys he
 * buys with this money.
 *
 * Now, you are Mark's best friend and have to help him buy as many
 * toys as possible.
 *
 * @see https://www.hackerrank.com/challenges/mark-and-toys
 */
object MarkAndToys {
  
  def buyMaxToys(money: Int, prices: List[Int]): Int = {
    def loop(toys: Int, moneyLeft: Int, prices: List[Int]): Int =
      if (moneyLeft < prices.head) toys
      else loop(toys + 1, moneyLeft - prices.head, prices.tail)

    loop(0, money, prices.sorted)
  }

  def main(args: Array[String]) {
    val lines = io.Source.stdin.getLines().toArray
    printResult(lines)
  }

  def printResult(lines: Array[String]): Unit = {
    val i1 = lines(0)
    val i2 = lines(1)
    val money = i1.split(" ").map(_.toInt).toList.tail.head
    val prices = i2.split(" ").map(_.toInt).toList
    val result = buyMaxToys(money, prices)
    println(result)
  }
}