package greedy

/**
 * You and your K-1 friends want to buy N flowers. Flower number i has cost c_i.
 * Unfortunately the seller does not want just one customer to buy a lot of
 * flowers, so he tries to change the price of flowers for customers who have
 * already bought some flowers. More precisely, if a customer has already
 * bought x flowers, he should pay (x + 1) * c_i dollars to buy flower number i.
 *
 * You and your K-1 friends want to buy all N flowers in such a way that you
 * spend the least amount of money. You can buy the flowers in any order.
 *
 * @see https://www.hackerrank.com/challenges/flowers
 */
object Flowers {

  def minimiseMoneySpent(flowers: List[Int], k: Int): Int = {
    def loop(flowers: List[Int], turn: Int, penalty: Int, acc: Int): Int = {
      if (flowers.isEmpty) acc
      else if (turn == k) loop(flowers.tail, 1, penalty + 1, acc + penalty * flowers.head)
      else loop(flowers.tail, turn + 1, penalty, acc + penalty * flowers.head)
    }

    loop(flowers.sortWith(_ > _), 1, 1, 0)
  }

  def main(args: Array[String]) {
    val lines = io.Source.stdin.getLines().toList
    printResult(lines)
  }

  def printResult(ls: List[String]): Unit = {
    val k = ls.head.split(" ").map(_.toInt).toList.tail.head
    val flowers = ls.tail.head.split(" ").map(_.toInt).toList
    val result = minimiseMoneySpent(flowers, k)
    println(result)
  }
}