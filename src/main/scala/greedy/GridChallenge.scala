package greedy

/**
 * Given a squared sized grid G of size N in which each cell has a lowercase
 * letter. Denote the character in the ith row and in the jth column as G[i][j].
 *
 * You can perform one operation as many times as you like: Swap two column
 * adjacent characters in the same row G[i][j] and G[i][j+1] for all valid i,j.
 *
 * Is it possible to rearrange the grid such that the following condition is
 * true?
 *
 * G[i][1] <= G[i][2] <= ... <= G[i][N] for 1 <= i <= N and
 * G[1][j] <= G[2][j] <= ... <= [N][j] for 1 <= i <= N
 * In other words, is it possible to rearrange the grid such that every row and
 * every column is lexicographically sorted?
 *
 * Note: c1 <= c2, if letter c1 is equal to c2 or is before c2 in the alphabet.
 *
 * @see https://www.hackerrank.com/challenges/grid-challenge
 */
object GridChallenge {

  class Matrix(val elements: Vector[Vector[Char]]) {
    def nRows: Int = elements.length

    override def toString() = elements.mkString(" ")
  }

  object Matrix {
    def apply(lines: Vector[String]): Matrix = {
      val rows: Vector[Vector[Char]] = (lines map { line: String =>
        line.toVector.sorted
      }).toVector
      new Matrix(rows)
    }
  }

  def possibleRearrangement(matrix: Matrix): Boolean = {
    canRearrange(matrix.elements) && canRearrange(matrix.elements.transpose)
  }

  def canRearrange(elements: Vector[Vector[Char]]): Boolean = {
    def canRearrange(row: Vector[Char]): Boolean = {
      row sliding(2) forall {
        case Vector(a)    => true
        case Vector(a, b) => a.compare(b) <= 0
      }
    }

    elements forall {
      canRearrange(_)
    }
  }

  def main(args: Array[String]) {
    val lines = io.Source.stdin.getLines().toVector
    val parsedInput = parseInput(lines.tail)
    parsedInput.foreach(input => printResult(input))
  }

  def parseInput(lines: Vector[String]): List[Matrix] = {
    def loop(lines: Vector[String], result: List[Matrix]): List[Matrix] = {
      if (lines.isEmpty) result
      else {
        val n = lines.head.toInt
        val matrix = Matrix(lines.tail.take(n))
        loop(lines.drop(n + 1), matrix :: result)
      }
    }
    loop(lines, Nil).reverse
  }

  def printResult(matrix: Matrix): Unit = {
    val result = possibleRearrangement(matrix)
    result match {
      case true => println("YES")
      case false => println("NO")
    }
  }

}