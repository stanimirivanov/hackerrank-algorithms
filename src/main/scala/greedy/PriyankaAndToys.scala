package greedy

/**
 * Little Priyanka visited a kids' shop. There are N toys and their
 * weight is represented by an array W = [w1, w2, ..., wN]. Each toy
 * costs 1 unit, and if she buys a toy with weight w′, then she
 * can get all other toys whose weight lies between [w', w' + 4]
 * (both inclusive) free of cost.
 *
 * @see https://www.hackerrank.com/challenges/priyanka-and-toys
 */
object PriyankaAndToys {

  def minimizeUnits(weights: List[Int]): Int = {
    def loop(count: Int, w: Int, weights: List[Int]): Int =
      if (weights.isEmpty) count
      else if (weights.head < w - 4) loop(count + 1, weights.head, weights.tail)
      else loop(count, w, weights.tail)

    val sortedWeights = weights.sortWith(_ > _)
    loop(1, sortedWeights.head, sortedWeights.tail)
  }

  def main(args: Array[String]) {
    val lines = io.Source.stdin.getLines().toArray
    printResult(lines)
  }

  def printResult(lines: Array[String]): Unit = {
    val input = lines(1)
    val weights = input.split(" ").map(_.toInt).toList
    val result = minimizeUnits(weights)
    println(result)
  }
}