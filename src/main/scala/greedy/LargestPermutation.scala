package greedy

/**
 * You are given an array of N integers which is a permeation of the first N natural
 * numbers. You can swap any two elements of the array. You can make at most K swaps.
 * What is the largest permutation, in numerical order, you can make?
 *
 * @see https://www.hackerrank.com/challenges/largest-permutation
 */
object LargestPermutation {

  type Permutation = Vector[Int]

  def largestPermutation(perm: Permutation, maxSwaps: Int): Permutation = {
    def loop(start: Permutation, end: Permutation, k: Int): Permutation = {
      if (k == maxSwaps || end.isEmpty) start ++ end
      else {
        val (x, idx) = end.zipWithIndex.maxBy(_._1)
        idx match {
          case 0 => loop(start :+ x, end.tail, k)
          case _ => loop(start :+ x, end.updated(idx, end.head).tail, k + 1)
        }
      }
    }

    loop(Vector(), perm, 0)
  }

  def main(args: Array[String]) {
    val lines = io.Source.stdin.getLines().toList
    printResult(lines)
  }

  def printResult(lines: List[String]): Unit = {
    val Array(_, maxSwaps) = lines(0).split(" ").map(_.toInt)
    val perm = lines(1).split(" ").map(_.toInt).toVector
    val result = largestPermutation(perm, maxSwaps)
    println(result.mkString(" "))
  }

}