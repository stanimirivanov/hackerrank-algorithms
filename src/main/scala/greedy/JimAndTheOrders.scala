package greedy

/**
 * In Jim's Burger, n hungry burger fans are ordering burgers. The i-th order
 * is placed by the ith fan at t_i time and it takes d_i time to procees.
 * What is the order in which the fans will get their burgers?
 *
 * @see https://www.hackerrank.com/challenges/jim-and-the-orders
 */
object JimAndTheOrders {

  case class Order(val id: Int, val arrival: Int, val proceesing: Int) extends Ordered[Order] {

    def time: Int = arrival + proceesing

    def compare(that: Order): Int = {
      if (this.time < that.time) -1
      else if (this.time > that.time) 1
      else if (this.id < that.id) -1
      else 1
    }

    override def toString() = id.toString
  }

  object Order {
    def apply(line: String, id: Int): Order = {
      val Array(a, b) = line.split(" ").map(_.toInt)
      new Order(id, a, b)
    }
  }

  def placeOrders(orders: List[Order]): List[Order] =
    orders.sorted

  def main(args: Array[String]) {
    val lines = io.Source.stdin.getLines().toList
    printResult(lines.tail)
  }

  def printResult(lines: List[String]): Unit = {
    val orders = lines.zipWithIndex map { case (line, id) => Order(line, id + 1) }
    val result = placeOrders(orders)
    println(result.mkString(" "))
  }

}