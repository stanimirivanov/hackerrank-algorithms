package search

/**
 * Sunny and Johnny together have M dollars and want to spend the amount
 * at an ice cream parlour. The parlour offers N flavors, and they want to
 * choose 2 flavors so that they end up spending the whole amount.
 *
 * You are given a list of cost of these N flavors. The cost of i-th flavor is
 * denoted by (ci). You have to display the indices of two flavors whose sum is M.
 *
 * @see https://www.hackerrank.com/challenges/icecream-parlor
 */
object IceCreamParlor {

  def spend(money: Int, prices: List[Int]): List[Int] = {
    val indexedResult = combinations(prices.zipWithIndex, 2).dropWhile { ps =>
      sum(ps) != money
    }
    indexedResult.next map { case (price, index) => index + 1 }
  }

  def combinations[A](l: List[A], n: Int): Iterator[List[A]] = {
    def go(l: List[A], n: Int): Iterator[List[A]] = n match {
      case _ if n < 0 || l.lengthCompare(n) < 0 => Iterator.empty
      case 0                                    => Iterator(List.empty)
      case 1                                    => l.iterator.map(a => (a :: Nil))
      case n => l.tails.map({
        case Nil     => Nil
        case x :: xs => go(xs, n - 1).map(ys => x :: ys)
      }).flatten
    }
    go(l.distinct, n)
  }

  def sum(indexedPrices: List[(Int, Int)]): Int =
    indexedPrices.foldLeft(0)((acc, t) => t match { case (price, index) => acc + price })

  def main(args: Array[String]) {
    val lines = io.Source.stdin.getLines().toArray
    lines.drop(1).grouped(3).foreach(ls => printResult(ls))
  }

  def printResult(ls: Array[String]): Unit = {
    val money = ls(0).toInt
    val prices = ls(2).split(" ").map(_.toInt).toList
    val result = spend(money, prices)
    println(result.mkString(" "))
  }
}