package search

/**
 * You are given an array of size N and another integer M.Your target is to
 * find the maximum value of sum of subarray modulo M.
 *
 * Subarray is a continuous subset of array elements.
 *
 * Note that we need to find the maximum value of (Sum of Subarray)%M , where
 * there are N ?(N+1)/2 possible subarrays.
 *
 * @see https://www.hackerrank.com/challenges/maximise-sum
 */
object MaximiseSum {

  case class MaxSumResult(maxSum: BigInt, startIdx: Int, endIdx: Int) {
    override def toString() = maxSum.toString
  }

  def maximiseSum(xs: Vector[BigInt], m: BigInt): BigInt = {
//    val memo = (xs.indices zip xs).toMap
//
//    def loop(max: BigInt, idx: Int, startIdx: Int, result: MaxSumResult): MaxSumResult = {
//      if
//    }
    BigInt(0)
  }

  def main(args: Array[String]) {
    val lines = io.Source.stdin.getLines().toArray
    lines.tail.grouped(2).foreach(printResult(_))
  }

  def printResult(lines: Array[String]): Unit = {
    val Array(_, m) = lines(0).split(" ").map(BigInt(_))
    val xs = lines(1).split(" ").map(BigInt(_)).toVector
    val result = maximiseSum(xs, m)
    println(result)
  }
}