package search

/**
 * Numeros, The Artist, had two lists A and B, such that, B was a permutation of A.
 * Numeros was very proud of these lists. Unfortunately, while transporting them
 * from one exhibition to another, some numbers from List A got left out. Can you
 * find out the numbers missing from A?
 *
 * Notes:
 * - If a number occurs multiple times in the lists, you must ensure that the
 *    frequency of that number in both the lists is the same. If that is not the
 *    case, then it is also a missing number.
 * - You have to print all the missing numbers in ascending order.
 * - Print each missing number once, even if it is missing multiple times.
 * - The difference between maximum and minimum number in the list B is less than
 *    or equal to 100.
 *
 * @see https://www.hackerrank.com/challenges/missing-numbers
 */
object MissingNumbers {

  def findMissingNumbers(as: List[Int], bs: List[Int]): List[Int] =
    bs.diff(as).distinct.sorted

  def main(args: Array[String]) {
    val lines = io.Source.stdin.getLines().toArray
    printResult(lines)
  }

  def printResult(lines: Array[String]): Unit = {
    val as = lines(1).split(" ").map(_.toInt).toList
    val bs = lines(3).split(" ").map(_.toInt).toList
    val result = findMissingNumbers(as, bs)
    println(result.mkString(" "))
  }
}