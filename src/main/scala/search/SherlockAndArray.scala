package search

/**
 * Watson gives an array A1,A2...AN to Sherlock. Then he asks him to find if
 * there exists an element in the array, such that, the sum of elements on its
 * left is equal to the sum of elements on its right. If there are no elements
 * to left/right, then sum is considered to be zero.
 *
 * Formally, find an i, such that, A1 + A2 ... Ai-1 = Ai+1 + Ai+2 ... AN.
 *
 * @see https://www.hackerrank.com/challenges/sherlock-and-array
 */
object SherlockAndArray {

  def split(nums: Vector[Long]): Option[Int] = {
    def loop(index: Int, left: Long, right: Long, total: Long): Option[Int] = {
      if (index == nums.size - 1) None
      else {
        val leftSum = left + nums(index - 1)
        val rightSum = total - nums(index) - leftSum
        if (leftSum == rightSum) Some(index)
        else loop(index + 1, leftSum, rightSum, total)
      }
    }

    if (nums.size == 1) Some(0)
    else if (nums.size == 2) None
    else {
      val total = nums.sum
      loop(1, 0, total, total)
    }
  }

  def main(args: Array[String]) {
    val lines = io.Source.stdin.getLines().toList
    lines.tail.grouped(2).foreach(ls => printResult(ls.tail.head))
  }

  def printResult(s: String): Unit = {
    val nums = s.split(" ").map(_.toLong).toVector
    val result = split(nums)
    result match {
      case Some(_) => println("YES")
      case None    => println("NO")
    }
  }
}