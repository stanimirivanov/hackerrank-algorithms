package greedy

import org.junit.runner.RunWith
import org.scalatest.FlatSpec
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class CuttingBoardsSpec extends FlatSpec {

  "Cutting Boards" should "pass sample test cases" in {
    val inputs = List[(List[BigInt], List[BigInt])]((List(BigInt(2)), List(BigInt(1))),
      (List(BigInt(2), BigInt(1), BigInt(3), BigInt(1), BigInt(4)), List(BigInt(4), BigInt(1), BigInt(2))))
    val expected = List[BigInt](BigInt(4), BigInt(42))

    val result = inputs.zip(expected).forall { t =>
      CuttingBoards.minimizeCutting(t._1._1, t._1._2) % 1000000007 == t._2
    }
    assert(result === true)
  }

  "Cutting Boards" should "pass sample test case 7" in {
    val PATH = getClass.getResource("").getPath
    val source = scala.io.Source.fromFile(PATH + "cuttingBoards-input7.txt")
    val lines = source.getLines.toList
    source.close()

    val inputs = lines.tail.grouped(3).map(ls => (ls.tail.head.split(" ").map(BigInt(_)).toList, ls.tail.tail.head.split(" ").map(BigInt(_)).toList)).toList
    val expected = List[BigInt](BigInt(864831319), BigInt(630156434), BigInt(451644751), BigInt(905689451), BigInt(50712649), BigInt(995771133), BigInt(342621032), BigInt(790331457), BigInt(404790557), BigInt(917472581))

    val result = inputs.zip(expected).forall { t =>
      CuttingBoards.minimizeCutting(t._1._1, t._1._2) % 1000000007 == t._2
    }
    assert(result === true)
  }

}