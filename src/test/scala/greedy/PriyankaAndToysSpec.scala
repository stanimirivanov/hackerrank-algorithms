package greedy

import org.junit.runner.RunWith
import org.scalatest.FlatSpec
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class PriyankaAndToysSpec extends FlatSpec {

  "Priyanka and Toys" should "pass test cases" in {
    val inputs = List[List[Int]](List(1, 2, 3, 17, 10))
    val expected = List[Int](3)

    val result = inputs.zip(expected).forall { t =>
      PriyankaAndToys.minimizeUnits(t._1) == t._2
    }
    assert(result === true)
  }

}