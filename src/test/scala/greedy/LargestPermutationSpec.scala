package greedy

import org.junit.runner.RunWith
import org.scalatest.FlatSpec
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class LargestPermutationSpec extends FlatSpec {

  "Largest Permutation" should "pass test case 0" in {
    val maxSwaps = 1
    val perm = Vector(4, 2, 3, 5, 1)
    val result = LargestPermutation.largestPermutation(perm, maxSwaps)
    assert(result === Vector(5, 2, 3, 4, 1))
  }

  "Largest Permutation" should "pass test case 1" in {
    val maxSwaps = 1
    val perm = Vector(2, 1, 3)
    val result = LargestPermutation.largestPermutation(perm, maxSwaps)
    assert(result === Vector(3, 1, 2))
  }

  "Largest Permutation" should "pass test case 2" in {
    val maxSwaps = 1
    val perm = Vector(2, 1)
    val result = LargestPermutation.largestPermutation(perm, maxSwaps)
    assert(result === Vector(2, 1))
  }

}