package greedy

import org.junit.runner.RunWith
import org.scalatest.FlatSpec
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class TwoArraysSpec extends FlatSpec {

  "Two Arrays" should "pass test cases" in {
    val inputs = List[(List[Int], List[Int], Int)]((List(2, 1, 3), List(7, 8, 9), 10), 
          (List(1, 2, 2, 1), List(3, 3, 3, 4), 5))
    val expected = List[Option[(List[Int], List[Int])]](Some(List(3, 2, 1),List(7, 8, 9)), None)

    val result = inputs.zip(expected).forall { t =>
      TwoArrays.findPermutation(t._1._1, t._1._2, t._1._3) == t._2
    }
    assert(result === true)
  }

}