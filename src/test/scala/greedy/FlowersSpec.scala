package greedy

import org.junit.runner.RunWith
import org.scalatest.FlatSpec
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class FlowersSpec extends FlatSpec {

  "Flowers" should "pass test cases" in {
    val inputs = List[(List[Int], Int)]((List(2, 5, 6), 3), 
        (List(2, 5, 6), 2))
    val expected = List[Int](13, 15)

    val result = inputs.zip(expected).forall { t =>
      Flowers.minimiseMoneySpent(t._1._1, t._1._2) == t._2
    }
    assert(result === true)
  }

}