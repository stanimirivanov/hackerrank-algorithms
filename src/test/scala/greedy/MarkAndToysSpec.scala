package greedy

import org.junit.runner.RunWith
import org.scalatest.FlatSpec
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class MarkAndToysSpec extends FlatSpec {

  "Mark and Toys" should "pass test cases" in {
    val inputs = List[(Int, List[Int])]((50, List(1, 12, 5, 111, 200, 1000, 10)))
    val expected = List[Int](4)

    val result = inputs.zip(expected).forall { t =>
      MarkAndToys.buyMaxToys(t._1._1, t._1._2) == t._2
    }
    assert(result === true)
  }

}