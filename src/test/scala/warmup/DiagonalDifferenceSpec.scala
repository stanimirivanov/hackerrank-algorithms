package warmup

import org.junit.runner.RunWith
import org.scalatest.FlatSpec
import org.scalatest.junit.JUnitRunner
import warmup.DiagonalDifference.Matrix

@RunWith(classOf[JUnitRunner])
class DiagonalDifferenceSpec extends FlatSpec {

  "Diagonal Difference" should "pass test cases" in {
    val inputs = List[List[String]](List("11 2 4", "4 5 6", "10 8 -12"))
    val expected = List[Int](15)

    val result = inputs.zip(expected).forall { t =>
      val m = Matrix(t._1)
      DiagonalDifference.diagonalDifference(m) == t._2
    }
    assert(result === true)

  }
}