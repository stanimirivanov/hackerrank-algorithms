package sorting

import org.junit.runner.RunWith
import org.scalatest.FlatSpec
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class AlmostSortedSpec extends FlatSpec {

  "Almost Sorted" should "pass sample test case" in {
    val inputs = List[Vector[Int]](Vector(4, 2), Vector(3, 1, 2), Vector(1, 5, 4, 3, 2, 6))
    val expected = List[Seq[String]](Seq("yes", "swap 1 2"), Seq("no"), Seq("yes", "reverse 2 5"))

    val result = inputs.zip(expected).forall { t =>
      AlmostSorted.isAlmostSorted(t._1) == t._2
    }
    assert(result === true)
  }

  "Almost Sorted" should "pass test case 13" in {
    val PATH = getClass.getResource("").getPath
    val sourceInput = scala.io.Source.fromFile(PATH + "almostSorted-input13.txt")
    val lines = sourceInput.getLines.toArray
    val n = lines(0).toInt
    val xs = lines(1).split(" ").map(_.toInt).toVector
    sourceInput.close()

    val expected = Seq("yes", "swap 19429 37227")

    val result = AlmostSorted.isAlmostSorted(xs)
    assert(result === expected)
  }
}