package sorting

import org.junit.runner.RunWith
import org.scalatest.FlatSpec
import org.scalatest.junit.JUnitRunner

import scala.collection.mutable.Buffer

@RunWith(classOf[JUnitRunner])
class RunningTimeOfAlgorithmsSpec extends FlatSpec {

  "Running Time of Algorithms" should "pass test cases" in {
    val inputs = List[Buffer[Int]](Buffer(2, 1, 3, 1, 2), Buffer(1, 1, 2, 2, 3, 3, 5, 5, 7, 7, 9, 9), Buffer(10, 9, 8, 7, 6, 5, 4, 3, 2, 1))
    val expected = List[Int](4, 0, 45)

    val result = inputs.zip(expected).forall { t =>
      RunningTimeOfAlgorithms.insertionSort(t._1) == t._2
    }
    assert(result === true)
  }

}