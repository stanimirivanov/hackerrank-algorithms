package sorting

import org.junit.runner.RunWith
import org.scalatest.FlatSpec
import org.scalatest.junit.JUnitRunner

import scala.collection.mutable.Buffer

@RunWith(classOf[JUnitRunner])
class QuickSort2Spec extends FlatSpec {

  "Quick Sort 2 - Sorting" should "pass test cases" in {
    val inputs = List[List[Int]](List(5, 8, 1, 3, 7, 9, 2), List(9, 8, 6, 7, 3, 5, 4, 1, 2))
    val expected = List[List[List[Int]]](List(List(2, 3), List(1, 2, 3), List(7, 8, 9), List(1, 2, 3, 5, 7, 8, 9)),
      List(List(1, 2), List(4, 5), List(1, 2, 3, 4, 5), List(1, 2, 3, 4, 5, 6, 7), List(1, 2, 3, 4, 5, 6, 7, 8), List(1, 2, 3, 4, 5, 6, 7, 8, 9)))

    val result = inputs.zip(expected).forall { t =>
      QuickSort2.quicksort(t._1)._2 == t._2
    }
    assert(result === true)
  }

}