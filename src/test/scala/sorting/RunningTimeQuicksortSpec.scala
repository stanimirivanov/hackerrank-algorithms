package sorting

import org.junit.runner.RunWith
import org.scalatest.FlatSpec
import org.scalatest.junit.JUnitRunner

import scala.collection.mutable.Buffer

@RunWith(classOf[JUnitRunner])
class RunningTimeQuicksortSpec extends FlatSpec {

  "Quick Sort 1 - Partition" should "pass test cases" in {
    val inputs = List[String]("1 3 9 8 2 7 5", "10 9 8 7 6 5 4 3 2 1", "1 3 4 5 2 7 8 9 6 10")
    val expected = List[Int](1, 16, -16)

    val result = inputs.zip(expected).forall { t =>
      RunningTimeQuicksort.operationsDiff(t._1) == t._2
    }
    assert(result === true)
  }

}