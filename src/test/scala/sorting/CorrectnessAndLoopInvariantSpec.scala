package sorting

import org.junit.runner.RunWith
import org.scalatest.FlatSpec
import org.scalatest.junit.JUnitRunner

import scala.collection.mutable.Buffer

@RunWith(classOf[JUnitRunner])
class CorrectnessAndLoopInvariantSpec extends FlatSpec {

  "Correctness and the Loop Invariant" should "pass test cases" in {
    val inputs = List[Buffer[Int]](Buffer(4, 1, 3, 5, 6, 2))
    val expected = List[Buffer[Int]](Buffer(1, 2, 3, 4, 5, 6))

    val result = inputs.zip(expected).forall { t =>
      CorrectnessAndLoopInvariant.insertionSort(t._1)
      t._1 == t._2
    }
    assert(result === true)
  }

}