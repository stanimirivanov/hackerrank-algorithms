package sorting

import org.junit.runner.RunWith
import org.scalatest.FlatSpec
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class CountingSort3Spec extends FlatSpec {

  "Counting Sort 3" should "pass sample test case" in {
    val i1 = "4 that 3 be 0 to 1 be 5 question 1 or 2 not 4 is 2 to 4 the"
    val inputs = List[(Vector[Int], Int)]((i1.split(" ").toList.grouped(2).map(_.head.toInt).toVector, 100))

    val e1 = "1 3 5 6 9 10 10 10 10 10 10 10 10 10 10 10 10 10 10 10 10 10 10 10 10 10 10 10 10 10 10 10 10 10 10 10 10 10 10 10 10 10 10 10 10 10 10 10 10 10 10 10 10 10 10 10 10 10 10 10 10 10 10 10 10 10 10 10 10 10 10 10 10 10 10 10 10 10 10 10 10 10 10 10 10 10 10 10 10 10 10 10 10 10 10 10 10 10 10 10"
    val expected = List[Seq[Int]](e1.split(" ").map(_.toInt).toSeq)

    val result = inputs.zip(expected).forall { t =>
      CountingSort3.elementsLessOrEqual(t._1._1, t._1._2) == t._2
    }
    assert(result === true)
  }

}