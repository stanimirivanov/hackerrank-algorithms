package sorting

import org.junit.runner.RunWith
import org.scalatest.FlatSpec
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class ClosestNumbersSpec extends FlatSpec {

  "Closest Numbers" should "pass test cases" in {
    val inputs = List[List[Int]](List(-20, -3916237, -357920, -3620601, 7374819, -7330761, 30, 6246457, -6461594, 266854),
      List(-20, -3916237, -357920, -3620601, 7374819, -7330761, 30, 6246457, -6461594, 266854, -520, -470),
      List(5, 4, 3, 2))
    val expected = List[Seq[List[Int]]](Seq(List(-20, 30)), Seq(List(-520, -470), List(-20, 30)), Seq(List(2, 3), List(3, 4), List(4, 5)))

    val result = inputs.zip(expected).forall { t =>
      ClosestNumbers.closestNumbers(t._1) == t._2
    }
    assert(result === true)
  }

}