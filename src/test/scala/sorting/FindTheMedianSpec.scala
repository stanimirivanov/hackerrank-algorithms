package sorting

import org.junit.runner.RunWith
import org.scalatest.FlatSpec
import org.scalatest.junit.JUnitRunner

import scala.collection.mutable.Buffer

@RunWith(classOf[JUnitRunner])
class FindTheMedianSpec extends FlatSpec {

  "Find the Median" should "pass sample test case" in {
    val inputs = List[Array[Int]](Array(0, 1, 2, 4, 6, 5, 3))
    val expected = List[Int](3)

    val result = inputs.zip(expected).forall { t =>
      FindTheMedian.findMedian(t._1) == t._2
    }
    assert(result === true)
  }

  "Find the Median" should "pass test case 1" in {
    val PATH = getClass.getResource("").getPath
    val sourceInput = scala.io.Source.fromFile(PATH + "findMedian-input01.txt")
    val lines = sourceInput.getLines.toArray
    val xs = lines(1).split(" ").map(_.toInt).toArray
    sourceInput.close()

    val expected = 49

    val result = FindTheMedian.findMedian(xs)
    assert(result === true)
  }

}