package sorting

import org.junit.runner.RunWith
import org.scalatest.FlatSpec
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class SherlockAndWatsonSpec extends FlatSpec {

  "Closest Numbers" should "pass test cases" in {
    val inputs = List[(Vector[Int], List[Int], Int)]((Vector(1, 2, 3), List(0, 1, 2), 2))
    val expected = List[Seq[Int]](Seq(2, 3, 1))

    val result = inputs.zip(expected).forall { t =>
      SherlockAndWatson.queryAfterRotatins(t._1._1, t._1._2, t._1._3) == t._2
    }
    assert(result === true)
  }

}