package sorting

import org.junit.runner.RunWith
import org.scalatest.FlatSpec
import org.scalatest.junit.JUnitRunner

import scala.collection.mutable.Buffer

@RunWith(classOf[JUnitRunner])
class QuickSortInPlaceSpec extends FlatSpec {

  "Quicksort In-Place" should "pass test cases" in {
    val inputs = List[Array[Int]](Array(1, 3, 9, 8, 2, 7, 5))
    val expected = List[List[List[Int]]](List(List(1, 3, 2, 5, 9, 7, 8), List(1, 2, 3, 5, 9, 7, 8), List(1, 2, 3, 5, 7, 8, 9)))

    val result = inputs.zip(expected).forall { t =>
      QuickSortInPlace.quicksort(t._1) == t._2
    }
    assert(result === true)
  }

}