package sorting

import org.junit.runner.RunWith
import org.scalatest.FlatSpec
import org.scalatest.junit.JUnitRunner

import scala.collection.mutable.Buffer

@RunWith(classOf[JUnitRunner])
class QuickSort1Spec extends FlatSpec {

  "Quick Sort 1 - Partition" should "pass test cases" in {
    val inputs = List[List[Int]](List(4, 5, 3, 7, 2))
    val expected = List[List[Int]](List(3, 2, 4, 5, 7))

    val result = inputs.zip(expected).forall { t =>
      QuickSort1.partition(t._1) == t._2
    }
    assert(result === true)
  }

}