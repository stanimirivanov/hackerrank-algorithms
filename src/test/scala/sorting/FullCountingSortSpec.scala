package sorting

import org.junit.runner.RunWith
import org.scalatest.FlatSpec
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class FullCountingSortSpec extends FlatSpec {

  import sorting.FullCountingSort.Item

  "The Full Counting Sort" should "pass sample test case" in {
    val i1 = "0 ab 6 cd 0 ef 6 gh 4 ij 0 ab 6 cd 0 ef 6 gh 0 ij 4 that 3 be 0 to 1 be 5 question 1 or 2 not 4 is 2 to 4 the"
    val inputs = List[Vector[Item]](i1.split(" ").toArray.grouped(2).map(ls => Item(ls(0).toInt, ls(1))).toVector)

    val expected = List[String]("- - - - - to be or not to be - that is the question - - - -")

    val result = inputs.zip(expected).forall { t =>
      FullCountingSort.countingSort(t._1, 100) == t._2
    }
    assert(result === true)
  }

}