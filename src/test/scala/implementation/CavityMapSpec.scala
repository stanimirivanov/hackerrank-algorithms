package implementation

import org.junit.runner.RunWith
import org.scalatest.FlatSpec
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class CavityMapSpec extends FlatSpec {

  "Cavity Map" should "pass test cases" in {
    val input = Vector("1112".toVector, "1912".toVector, "1892".toVector, "1234".toVector)
    val expected = Seq("1112".toSeq, "1X12".toSeq, "18X2".toSeq, "1234".toSeq)

    val result = CavityMap.cavityMap(input)
    assert(result === expected)
  }

}