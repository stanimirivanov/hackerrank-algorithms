package implementation

import org.junit.runner.RunWith
import org.scalatest.FlatSpec
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class SherlockAndQueriesSpec extends FlatSpec {

  import scala.collection.mutable.Buffer
  
  "Sherlock and Queries" should "pass test case" in {
    val as = "1 2 3 4".split(" ").map(_.toLong).toBuffer
    val bs = "1 2 3".split(" ").map(_.toInt).toList
    val cs = "13 29 71".split(" ").map(_.toInt).toList
    
    val expected = "13 754 2769 1508 ".split(" ").map(_.toLong).toBuffer
    
    val result = SherlockAndQueries.decode(as, bs, cs)
    assert(result === expected)
  }

}