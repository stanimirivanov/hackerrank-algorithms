package implementation

import org.junit.runner.RunWith
import org.scalatest.FlatSpec
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class ServiceLaneSpec extends FlatSpec {

  "Cavity Map" should "pass test cases" in {
    val v1 = Vector(2, 3, 1, 2, 3, 2, 3, 3)
    val v2 = Vector(1, 2, 2, 2, 1)
    val inputs = List[(Vector[Int], Int, Int)](
      (v1, 0, 3), (v1, 4, 6), (v1, 6, 7), (v1, 3, 5), (v1, 0, 7),
      (v2, 2, 3), (v2, 1, 4), (v2, 2, 4), (v2, 2, 4), (v2, 2, 3))

    val expected = List[Int](
        1, 2, 3, 2, 1,
        2, 1, 1, 1, 2)

    val result = inputs.zip(expected).forall { t =>
      val pass = ServiceLane.passSegment(t._1._1, t._1._2, t._1._3)
      pass == t._2
    }
    assert(result === true)
  }

}