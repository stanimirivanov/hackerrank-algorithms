package implementation

import org.junit.runner.RunWith
import org.scalatest.FlatSpec
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class CutTheSticksSpec extends FlatSpec {

  "Cut the Sticks" should "pass test cases" in {
    val inputs = List[List[Int]](List(5, 4, 4, 2, 2, 8), List(1, 2, 3, 4, 3, 3, 2, 1))
    val expected = List[List[Int]](List(6, 4, 2, 1), List(8, 6, 4, 1))

    val result = inputs.zip(expected).forall { t =>
      val cut = CutTheSticks.cut(t._1)
      cut == t._2
    }
    assert(result === true)
  }

}