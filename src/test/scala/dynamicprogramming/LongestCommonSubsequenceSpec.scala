package dynamicprogramming

import org.junit.runner.RunWith
import org.scalatest.FlatSpec
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class LongestCommonSubsequenceSpec extends FlatSpec {

  def lcsr(a: Vector[Int], b: Vector[Int]): Vector[Int] = {
    if (a.isEmpty || b.isEmpty)
      Vector.empty
    else if (a.head == b.head)
      a.head +: lcsr(a.tail, b.tail)
    else {
      val case1 = lcsr(a.tail, b)
      val case2 = lcsr(a, b.tail)
      if (case1.length > case2.length) case1 else case2
    }
  }

  "Longest Common Subsequence" should "pass sample test case" in {
    val inputs = List[(Vector[Int], Vector[Int])]((toIntVector("1 2 3 4 1"), toIntVector("3 4 1 2 1 3")))
    val expected = List[Vector[Int]](toIntVector("1 2 3"))

    val result = inputs.zip(expected).forall { t =>
      val lcs = LongestCommonSubsequence.lcs(t._1._1, t._1._2)
      lcs == t._2
    }
    assert(result === true)
  }

  "Longest Common Subsequence" should "pass test case #2" in {
    val inputs = List[(Vector[Int], Vector[Int])](
      (
        toIntVector("16 27 89 79 60 76 24 88 55 94 57 42 56 74 24 95 55 33 69 29 14 7 94 41 8 71 12 15 43 3 23 49 84 78 73 63 5 46 98 26 40 76 41 89 24 20 68 14 88 26"),
        toIntVector("27 76 88 0 55 99 94 70 34 42 31 47 56 74 69 46 93 88 89 7 94 41 68 37 8 71 57 15 43 89 43 3 23 35 49 38 84 98 47 89 73 24 20 14 88 75")))
    val expected = List[Vector[Int]](toIntVector("27 76 88 55 94 42 56 74 69 7 94 41 8 71 15 43 3 23 49 84 98 89 24 20 14 88"))

    val result = inputs.zip(expected).forall { t =>
      val lcs = LongestCommonSubsequence.lcs(t._1._1, t._1._2)
      val lcsTable = LongestCommonSubsequence.buildLcsTable(t._1._1, t._1._2)
      lcs == t._2
    }
    assert(result === true)
  }

  def toIntVector(s: String): Vector[Int] = s.split(" ").toVector.map(_.toInt)
}