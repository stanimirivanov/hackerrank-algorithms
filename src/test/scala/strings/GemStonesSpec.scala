package strings

import org.junit.runner.RunWith
import org.scalatest.FlatSpec
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class GemStonesSpec extends FlatSpec {

  "Gem Stones" should "pass test cases" in {
    val inputs = List[List[String]](List("abcdde", "baccd", "eeabg"))
    val expected = List[Set[String]](Set("a", "b"))

    val result = inputs.zip(expected).forall { t =>
      GemStones.gemElementss(t._1).mkString == t._2.mkString
    }
    assert(result === true)
  }

}