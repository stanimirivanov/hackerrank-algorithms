package strings

import org.junit.runner.RunWith
import org.scalatest.FlatSpec
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class PangramsSpec extends FlatSpec {

  "Pangrams" should "pass test cases" in {
    val inputs = List[String]("We promptly judged antique ivory buckles for the next prize", "We promptly judged antique ivory buckles for the prize")
    val expected = List[Boolean](true, false)

    val result = inputs.zip(expected).forall { t =>
      Pangrams.isPangram(t._1) == t._2
    }
    assert(result === true)
  }

}