package strings

import org.junit.runner.RunWith
import org.scalatest.FlatSpec
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class PalindromeIndexSpec extends FlatSpec {

  "Palindrome Index" should "pass test cases" in {
    val inputs = List[String]("aaab", "baa", "aaa")
    val expected = List[Int](3, 0, -1)

    val result = inputs.zip(expected).forall { t =>
      PalindromeIndex.palindromeIndex(t._1) == t._2
    }
    assert(result === true)
  }

}