package strings

import org.junit.runner.RunWith
import org.scalacheck._
import org.scalatest.PropSpec
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class SherlockAndAnagramsSpec extends PropSpec {

  val validStrings: Gen[String] =
    for {
      n <- Gen.choose(2, 100)
      s <- Gen.listOfN(n, Gen.alphaChar) map {_.mkString.toLowerCase}
    } yield s

  property("Sherlock And Anagrams all anagrams spec.") {
    val valueProp = Prop.forAll(validStrings) { word =>
      val result = SherlockAndAnagrams.anagramicPairs(word)

      result forall {
        case (a, b) => SherlockAndAnagrams.anagrams(a, b)
      }
    }
    valueProp.check
  }

}