package strings

import org.junit.runner.RunWith
import org.scalatest.FlatSpec
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class GameOfThronesIISpec extends FlatSpec {

  "Game Of Thrones - II" should "pass test cases" in {
    val inputs = List[String]("aaabbbb", "cdcdcdcdeeeef")
    val expected = List[BigInt](BigInt(3), BigInt(90))

    val result = inputs.zip(expected).forall { t =>
      val count = GameOfThronesII.anagramCount(t._1)
      count == t._2
    }
    assert(result === true)
  }

}