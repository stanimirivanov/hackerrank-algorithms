package strings

import org.junit.runner.RunWith
import org.scalatest.FlatSpec
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class AnagramSpec extends FlatSpec {

  "Anagram" should "pass test cases" in {
    val inputs = List[String]("aaabbb", "ab", "abc", "mnop", "xyyx")
    val expected = List[Int](3, 1, -1, 2, 0)

    val result = inputs.zip(expected).forall { t =>
      Anagram.minimumSteps(t._1) == t._2
    }
    assert(result === true)
  }

}