package strings

import org.junit.runner.RunWith
import org.scalatest.FlatSpec
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class CommonChildSpec extends FlatSpec {

  "Common Child" should "pass test cases" in {
    val inputs = List[(String, String)](("HARRY", "SALLY"), ("AA", "BB"))
    val expected = List[Int](2, 0)

    val result = inputs.zip(expected).forall { t =>
      CommonChild.lcs(t._1._1, t._1._2) == t._2
    }
    assert(result === true)
  }

}