package strings

import org.junit.runner.RunWith
import org.scalacheck._
import org.scalatest.PropSpec
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class FunnyStringSpec extends PropSpec {


  val validStrings: Gen[List[Char]] =
    for {
      n <- Gen.choose(2, 10000)
      s <- Gen.listOfN(n, Gen.alphaChar)
    } yield s

  property("Funny String value spec.") {
    val valueProp = Prop.forAll(validStrings) { s =>
      isFunnyAlternative(s.toVector) == FunnyString.isFunny(s)
    }
    valueProp.check
  }

  def isFunnyAlternative(s: Vector[Char]): Boolean = {
    val r = s.reverse
    (0 until s.length - 2) forall {
      i => Math.abs(s(i) - s(i+1)) ==  Math.abs(r(i) - r(i+1))
    }
  }

}