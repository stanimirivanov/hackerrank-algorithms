package strings

import org.junit.runner.RunWith
import org.scalatest.FlatSpec
import org.scalatest.junit.JUnitRunner
import sorting.BiggerIsGreater
import scala.util.Try
import scala.io.Source
import scala.util.Success
import scala.util.Failure

@RunWith(classOf[JUnitRunner])
class BiggerIsGreaterSpec extends FlatSpec {

  val input1 = "biggerIsBetterInput1.txt"
  val output1 = "biggerIsBetterOutput1.txt"

  def readTextFile(fileName: String): Array[String] = {
    val content = Try {
      val source = Source.fromFile(fileName)
      val l = source.mkString
      source.close()
      l.split("\n\n")
    }

    content match {
      case Success(lines)   => lines
      case Failure(failure) => { println(failure); Array.empty }
    }
  }

  "Bigger is Greater" should "pass test case #2" in {
    val inputs = readTextFile(input1).toList
    val expected = readTextFile(output1).toList

    val result = inputs.zip(expected).forall { t =>
      val res = BiggerIsGreater.lexicoBigger(t._1)
      res match {
        case Some(answer) =>
          answer == t._2
        case None =>
          println("no answer" == t._2)
          println(" ")
          "no answer" == t._2
      }
    }
    assert(result === true)
  }

  "Bigger is Greater" should "pass sample test cases" in {
    val inputs = List[String]("ab", "bb", "hefg", "gojh")
    val expected = List[Option[String]](Some("ba"), None, Some("hegf"), Some("hgjo"))

    val result = inputs.zip(expected).forall { t =>
      BiggerIsGreater.lexicoBigger(t._1) == t._2
    }
    assert(result === true)
  }

}