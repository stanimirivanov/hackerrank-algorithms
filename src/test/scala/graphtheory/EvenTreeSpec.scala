package graphtheory

import org.junit.runner.RunWith
import org.scalatest.FlatSpec
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class EvenTreeSpec extends FlatSpec {

  "Even Tree" should "pass test cases" in {
    val inputs = List[List[String]](List("2 1", "3 1", "4 3", "5 2", "6 1", "7 2", "8 6", "9 8", "10 8"),
      List("2 1", "3 1", "4 3", "5 2", "6 5", "7 1", "8 1", "9 2", "10 7", "11 10", "12 3", "13 7", "14 8", "15 12", "16 6", "17 6", "18 10", "19 1", "20 8"))
    val expected = List[Int](2, 4)

    val result = inputs.zip(expected).forall { t =>
      val g = EvenTree.buildGraph(t._1)
      val res = EvenTree.evenTrees(g)
      res == t._2
    }
    assert(result === true)
  }

}