package search

import implementation.Encryption
import org.junit.runner.RunWith
import org.scalatest.FlatSpec
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class EncryptionSpec extends FlatSpec {

  "Encryption" should "pass test cases" in {
    val inputs = List[String]("haveaniceday", "feedthedog", "chillout")
    val expected = List[String]("hae and via ecy", "fto ehg ee dd", "clu hlt io")

    val result = inputs.zip(expected).forall { t =>
      val decoded = Encryption.decode(t._1)
      decoded == t._2
    }
    assert(result === true)
  }

}