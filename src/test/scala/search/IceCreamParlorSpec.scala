package search

import org.junit.runner.RunWith
import org.scalatest.FlatSpec
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class IceCreamParlorSpec extends FlatSpec {

  "Ice Cream Parlor" should "pass test cases" in {
    val inputs = List[(Int, List[Int])]((4, List(1, 4, 5, 3, 2)),(4, List(2, 2, 4, 3)))
    val expected = List[List[Int]](List(1, 4), List(1, 2))

    val result = inputs.zip(expected).forall { t =>
      val cut = IceCreamParlor.spend(t._1._1, t._1._2)
      cut == t._2
    }
    assert(result === true)
  }

}