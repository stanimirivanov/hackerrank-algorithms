package search

import org.junit.runner.RunWith
import org.scalatest.FlatSpec
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class MissingNumbersSpec extends FlatSpec {

  "Missing Numbers" should "pass test cases" in {
    val inputs = List[(List[Int], List[Int])]((List(203, 204, 205, 206, 207, 208, 203, 204, 205, 206), List(203, 204, 204, 205, 206, 207, 205, 208, 203, 206, 205, 206, 204)))
    val expected = List[List[Int]](List(204, 205, 206))

    val result = inputs.zip(expected).forall { t =>
      val cut = MissingNumbers.findMissingNumbers(t._1._1, t._1._2)
      cut == t._2
    }
    assert(result === true)
  }

}