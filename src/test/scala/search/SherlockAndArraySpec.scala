package search

import org.junit.runner.RunWith
import org.scalatest.FlatSpec
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class SherlockAndArraySpec extends FlatSpec {

  "Sherlock and Array" should "pass test cases" in {
    val inputs = List[Vector[Long]](Vector(1, 2, 3), Vector(1, 2, 3, 3))
    val expected = List[Option[Int]](None, Some(3))

    val result = inputs.zip(expected).forall { t =>
      val split = SherlockAndArray.split(t._1)
      split == t._2
    }
    assert(result === true)
  }

}