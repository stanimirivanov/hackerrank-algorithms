package bitmanipulation

import org.junit.runner.RunWith
import org.scalatest.FlatSpec
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class SansaAndXORSpec extends FlatSpec {

  "Sansa and XOR" should "pass test cases" in {
    val inputs = List[List[Int]](List(1, 2, 3))
    val expected = List[Int](2)

    val result = inputs.zip(expected).forall { t =>
      SansaAndXOR.xor(t._1) == t._2
    }
    assert(result === true)
  }

}