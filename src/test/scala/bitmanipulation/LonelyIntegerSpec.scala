package bitmanipulation

import org.junit.runner.RunWith
import org.scalacheck._
import org.scalatest.PropSpec
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class LonelyIntegerSpec extends PropSpec {

  val positiveIntList = for {
    size <- Gen.choose(1, 100)
    l <- Gen.listOfN(size, Gen.choose(1, 100))
  } yield l

  val largerInt = Gen.choose(101, 1000)

  property("Huge GCD value spec.") {
    val valueProp = Prop.forAll(positiveIntList, largerInt) { (xs: List[Int], y: Int) =>
      val lonelyIntegerList = y :: xs ::: xs
      val lonelyInteger = LonelyInteger.findLonelyInt(lonelyIntegerList)
      lonelyInteger == y
    }
    valueProp.check
  }

}